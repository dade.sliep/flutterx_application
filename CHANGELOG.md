## 2.3.1

* Improve routers

## 2.3.0

* Fix html_renderer

## 2.2.9

* Include error handling in TaskViewModel, improve application, add messages, fix fragment args

## 2.2.6

* Improve FlutterRenderer

## 2.2.3

* Add ErrorHandler

## 2.2.2

* Add ExpandableTile

## 2.2.1

* Improve AnchoredOverlay

## 2.2.0

* Move FragmentManager outside FragmentHost

## 2.1.9

* Improve fragment/navigation

## 2.1.6

* Add UI utilities

## 2.1.4

* Improve fragment navigation

## 2.1.1

* Add access control

## 2.1.0

* Improve FragmentManager

## 2.0.12

* Add pointer interceptor on modal

## 2.0.11

* Update flutter

## 2.0.10

* Fix fragment

## 2.0.9

* Improve fragment & code

## 2.0.7

* Improve code style & TokenHandler

## 2.0.6

* Improve RouteAwareObserver

## 2.0.5

* Improve RouteAwareObserver

## 2.0.4

* Improve router

## 2.0.3

* Add RouteAwareObserver

## 2.0.2

* Fix initialization

## 2.0.0

* Refactoring

## 1.9.3

* Improve ResponsiveScreenContent

## 1.9.2

* Add context in app factory

## 1.9.1

* Fix initialization for android

## 1.9.0

* Improve application

## 1.8.9

* Move AlertDialogAction

## 1.8.8

* Improve FragmentHost

## 1.8.6

* Improve FooterView

## 1.8.5

* Improve AlertDialogAction

## 1.8.3

* Moved NumberField to flutterx_forms package

## 1.8.2

* Fix NumberTextField

## 1.8.1

* Improve AlertDialogAction

## 1.7.7

* Improve routing, fix NumberTextField

## 1.7.6

* Improve FragmentManager

## 1.7.5

* Improve TaskViewModel

## 1.7.3

* Improve ViewModels, fix imports

## 1.7.2

* Improve NumberTextField

## 1.7.1

* Fix NumberTextField

## 1.7.0

* Add NumberTextField

## 1.6.95

* Add FlutterHtmlRenderer

## 1.6.94

* Improve FragmentManager

## 1.6.9

* Improve ResponsiveScreenContent

## 1.6.8

* Add UI constants

## 1.6.7

* Improve ViewModel

## 1.6.6

* Fix ViewModel

## 1.6.5

* Improve TaskViewModel

## 1.6.4

* Improve Application, add features

## 1.6.3

* Improve ViewModel usage

## 1.6.2

* Fix ExpandableListView

## 1.6.1

* Improve code, add UI components

## 1.6.0

* Update dependencies

## 1.5.9

* Support LiveData 1.6.x

## 1.5.8

* Improve SimpleLoader

## 1.5.7

* Improve FragmentContainer

## 1.5.6

* Improve FragmentContainer

## 1.5.5

* Add BuildContext to ViewModel

## 1.5.4

* Add provider to ViewModel

## 1.5.3

* Fragment bug fixing

## 1.5.2

* Application refactoring

## 1.5.1

* Improve NoFocusNode

## 1.5.0

* Add FragmentStateObserver

## 1.4.91

* Fix fragments

## 1.4.9

* Fragment improvements

## 1.4.8

* Fragment improvements

## 1.4.7

* Fragment improvements, fixes

## 1.4.6

* Add arguments to fragments

## 1.4.5

* Improve MaterialStatefulWidget

## 1.4.4

* Add args to notify

## 1.4.3

* Fix clear ViewModels on app restart

## 1.4.2

* Fix dialog title parameter

## 1.4.1

* Improvements

## 1.4.0

* Add fragments

## 1.3.3

* Add possibility to get AppRoute from context

## 1.3.2

* Improvements

## 1.3.1

* Fix labels

## 1.3.0

* Fix labels

## 1.2.9

* Improvements

## 1.2.8

* Improve ViewModel

## 1.2.7

* Switched to flutter channel stable

## 1.2.6

* Add TaskViewModel

## 1.2.5

* Fix update url

## 1.2.4

* Code improvements

## 1.2.3

* Improve url strategy

## 1.2.2

* Fix listen new urls, add path url strategy by default

## 1.2.1

* Improvements

## 1.2.0

* Refactoring

## 1.1.13-dev

* Add provider to ViewModel factory

## 1.1.12-dev

* Fix dialog & bottom sheet routes with non nullable return type

## 1.1.11-dev

* Add WillPop management for Web

## 1.1.10-dev

* Fix ViewModel

## 1.1.9-dev

* Add NoFocusNode

## 1.1.8-dev

* Introducing ViewModel

## 1.1.7-dev

* Update dependencies

## 1.1.6

* Fix app restart

## 1.1.5

* Add openReplace route method

## 1.1.4

* Refactor navigation system

## 1.1.3

* Code refactoring

## 1.1.2

* Fix material stateful widget

## 1.1.1

* Add draggable bottom sheet content size parameter

## 1.1.0

* Initial stable release

## 1.0.9-dev

* Improve application integration.

## 1.0.8-dev

* Fix web integration.

## 1.0.7-dev

* Improve navigation.

## 1.0.6-dev

* Improve navigation.

## 1.0.5-dev

* Improve Themes.

## 1.0.4-dev

* Improve MaterialStatefulWidget.

## 1.0.3-dev

* Improve DialogRoute.

## 1.0.2-dev

* Add MaterialStatefulWidget.

## 1.0.1-dev

* Improve SystemUI integration.

## 1.0.0-dev

* Initial release.
