import 'package:example/localization/labels.dart';
import 'package:example/ui/activity_data_delegate.dart';
import 'package:example/ui/activity_data_insert.dart';
import 'package:example/ui/activity_fragment.dart';
import 'package:example/ui/activity_home.dart';
import 'package:example/ui/activity_login.dart';
import 'package:example/ui/activity_private.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_preferences/flutterx_preferences.dart';

void main() => runApplication(
    name: 'Flutterx Application Demo',
    routes: {
      HomeActivity.route,
      DataDelegateActivity.route,
      DataInsertActivity.route,
      LoginActivity.route,
      PrivateActivity.route,
      FragmentActivity.route,
      _UnknownRouteActivity.route,
    },
    onUnknownRoute: (route) => RouteInformation(
        uri: Uri.parse(_UnknownRouteActivity.route.location), state: {'location': route.uri.toString()}),
    initialize: (context) => HomeActivity.route,
    theme: _appTheme,
    locale: () => LocaleData<Labels>(labels: Labels.supportedLabels, onLabel: Labels.onLabel, storage: sessionStorage));

final _appTheme = ThemeData(
    primaryColor: Colors.blueGrey,
    colorScheme: ColorScheme.fromSwatch(
      primarySwatch: Colors.blueGrey,
    ),
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: Colors.blueGrey[600],
    ));

class _UnknownRouteActivity extends StatelessWidget {
  static final ActivityRoute<String?> route = ActivityRoute(
      location: '/404', builder: (context, args) => _UnknownRouteActivity._(location: args['location'] ?? '404'));

  final String location;

  const _UnknownRouteActivity._({required this.location});

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            const Text('404', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32)),
            const SizedBox(height: 48),
            Text('Requested page not found: $location'),
            const SizedBox(height: 24),
            ElevatedButton(
                onPressed: () async =>
                    // ignore: use_build_context_synchronously
                    await Navigator.of(context).maybePop() ? null : Application.of(context).restart(),
                child: const Text('Back')),
          ]),
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('location', location));
  }
}
