import 'package:flutter/material.dart';
import 'package:flutterx_application/flutterx_application.dart';

part 'labels_en.dart';
part 'labels_it.dart';

late Labels labels;

class Labels extends LabelInterface {
  static final Set<Labels> supportedLabels = {_it, _en};
  final String appName;
  final String chooseLanguage;
  final String navigation;
  final String Function(String result) navigationResult;
  final String insertData;
  final String openActivity;
  final String openReplaceActivity;
  final String openForwardActivity;
  final String openDialog;
  final String openBottomSheet;
  final String Function(int argument) contentText;

  const Labels._({
    required super.locale,
    required this.appName,
    required this.chooseLanguage,
    required this.navigation,
    required this.navigationResult,
    required this.insertData,
    required this.openActivity,
    required this.openReplaceActivity,
    required this.openForwardActivity,
    required this.openDialog,
    required this.openBottomSheet,
    required this.contentText,
  });

  static void onLabel(Labels value) => labels = value;
}
