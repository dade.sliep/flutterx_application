part of 'labels.dart';

final _it = Labels._(
  locale: const Locale('it', 'IT'),
  appName: 'Application Demo',
  chooseLanguage: 'Seleziona la tua lingua',
  navigation: 'Navigazione',
  navigationResult: (result) => 'Risultato navigazione:\n$result',
  insertData: 'Inserisci dati',
  openActivity: 'Apri activity',
  openReplaceActivity: 'Apri activity (sostituisci)',
  openForwardActivity: 'Apri activity (inoltro risultato)',
  openDialog: 'Apri dialog',
  openBottomSheet: 'Apri bottom sheet',
  contentText: (argument) => 'Hai premuto il bottone $argument volte!',
);
