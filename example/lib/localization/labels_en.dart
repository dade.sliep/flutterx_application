part of 'labels.dart';

final _en = Labels._(
  locale: const Locale('en', 'US'),
  appName: 'Application Demo',
  chooseLanguage: 'Choose your language',
  navigation: 'Navigation',
  navigationResult: (result) => 'Navigation result:\n$result',
  insertData: 'Insert data',
  openActivity: 'Open activity',
  openReplaceActivity: 'Open activity (replace)',
  openForwardActivity: 'Open activity (forward result)',
  openDialog: 'Open dialog',
  openBottomSheet: 'Open bottom sheet',
  contentText: (argument) => 'You pressed the button $argument times!',
);
