import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide DialogRoute;
import 'package:flutter/rendering.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_preferences/flutterx_preferences.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

LiveList<Color> _colors =
    LiveList(source: [Colors.white, ...Colors.primaries], elementEquality: const IdentityEquality<Color>());

class FragmentActivity extends StatefulWidget {
  static final ActivityRoute<void> route = ActivityRoute(
      title: (_) => 'Fragment', location: '/fragment', builder: (context, args) => const FragmentActivity._());

  const FragmentActivity._();

  @override
  State<FragmentActivity> createState() => _FragmentActivityState();
}

class _FragmentActivityState extends State<FragmentActivity> {
  final MutableLiveData<bool> _primary =
      StorageMutableLiveData.bool(defaultValue: true, key: 'test_primary', storage: sessionStorage);
  final MutableLiveData<bool> _storage =
      StorageMutableLiveData.bool(defaultValue: true, key: 'test_storage', storage: sessionStorage);
  final MutableLiveData<bool> _canPop =
      StorageMutableLiveData.bool(defaultValue: true, key: 'test_pop', storage: sessionStorage);

  // @override
  // void initState() {
  //   super.initState();
  //   Future.delayed(const Duration(seconds: 1), () async {
  //     for (final color in Colors.primaries) {
  //       _colors.add(color);
  //       await Future.delayed(const Duration(seconds: 5));
  //     }
  //   });
  // }

  @override
  Widget build(BuildContext context) => Scaffold(
      body: LiveDataBuilder<List<Color>>(
          data: _colors,
          builder: (context, colors) {
            final fragments = [...colors.map((color) => Fragment1(color: color))];
            return MultipleLiveDataBuilder.with2<bool, bool>(
                x1: _primary,
                x2: _storage,
                builder: (context, primary, storage) => FragmentHost(
                    key: const FragmentHostKey('fragments'),
                    primary: primary,
                    storage: storage ? sessionStorage : null,
                    fragments: fragments));
          }));
}

MutableLiveData<int> _shared = MutableLiveData(value: 0);

class Fragment1 extends Fragment {
  final Color color;

  Fragment1({required this.color}) : super(key: FragmentKey(color.hexString));

  @override
  FragmentState createState() => Fragment1State();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(ColorProperty('color', color));
  }
}

class Fragment1State extends FragmentState<Fragment1>
    with AutomaticKeepAliveClientMixin, ObserverMixin, StateObserver, FragmentStateObserver {
  int _buildCount = 0;

  @override
  bool get wantKeepAlive => true;

  @override
  void registerObservers() => _shared.observe(this, _onChanged);

  void _onChanged(int value) {
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    _buildCount++;
    final state = context.findAncestorStateOfType<_FragmentActivityState>()!;
    return SingleChildScrollView(
        child: Container(
            color: widget.color,
            alignment: Alignment.center,
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              const SizedBox(height: 24),
              Wrap(children: [
                Text('Fragment ${widget.key} $_buildCount args: ${args()}'),
                const SizedBox(width: 24),
                LiveDataBuilder<bool>(
                    data: manager.canGoBack,
                    builder: (context, enabled) => ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            splashFactory: NoSplash.splashFactory,
                            foregroundColor: Colors.black,
                            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                            side: const BorderSide()),
                        onPressed: enabled ? () => manager.back() : null,
                        child: const Text('Back'))),
                const SizedBox(width: 24),
                LiveDataBuilder<bool>(
                    data: manager.canGoForward,
                    builder: (context, enabled) => ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            splashFactory: NoSplash.splashFactory,
                            foregroundColor: Colors.black,
                            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                            side: const BorderSide()),
                        onPressed: enabled ? () => manager.forward() : null,
                        child: const Text('Forward'))),
                const SizedBox(width: 24),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        splashFactory: NoSplash.splashFactory,
                        foregroundColor: Colors.black,
                        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                        side: const BorderSide()),
                    onPressed: () => _shared.value++,
                    child: Text('COUNTER ${_shared.value}')),
              ]),
              Wrap(children: [
                LiveDataBuilder<bool>(
                    data: state._primary,
                    builder: (context, enabled) => ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            splashFactory: NoSplash.splashFactory,
                            foregroundColor: Colors.black,
                            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                            side: const BorderSide()),
                        onPressed: () => state._primary.value = !state._primary.value,
                        child: Text('Primary $enabled'))),
                const SizedBox(width: 24),
                LiveDataBuilder<bool>(
                    data: state._storage,
                    builder: (context, enabled) => ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            splashFactory: NoSplash.splashFactory,
                            foregroundColor: Colors.black,
                            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                            side: const BorderSide()),
                        onPressed: () => state._storage.value = !state._storage.value,
                        child: Text('Storage $enabled'))),
                const SizedBox(width: 24),
                LiveDataBuilder<bool>(
                    data: state._storage,
                    builder: (context, enabled) => ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            splashFactory: NoSplash.splashFactory,
                            foregroundColor: Colors.black,
                            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                            side: const BorderSide()),
                        onPressed: () => _colors.setTo(Colors.accents.toList()..shuffle()),
                        child: const Text('Change colors'))),
                const SizedBox(width: 24),
                LiveDataBuilder<bool>(
                    data: state._canPop,
                    builder: (context, enabled) => FragmentWillPopScope(
                        onWillPop: () => SynchronousFuture(enabled),
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.white,
                                splashFactory: NoSplash.splashFactory,
                                foregroundColor: Colors.black,
                                padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                                shape:
                                    const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                                side: const BorderSide()),
                            onPressed: () => state._canPop.value = !state._canPop.value,
                            child: Text('Can pop: $enabled')))),
              ]),
              LiveDataBuilder<List<Color>>(
                  data: _colors,
                  builder: (context, colors) => Column(
                      mainAxisSize: MainAxisSize.min,
                      children: colors
                          .map((color) => Padding(
                              padding: const EdgeInsets.all(12),
                              child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: color,
                                      splashFactory: NoSplash.splashFactory,
                                      foregroundColor: color.computeLuminance() < .5 ? Colors.white : Colors.black,
                                      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(12))),
                                      side: BorderSide(
                                          color: color.computeLuminance() < .5 ? Colors.white : Colors.black)),
                                  onPressed: () => manager.navigateToKey(FragmentKey(color.hexString),
                                      direction: ScrollDirection.forward, args: {'seconds': DateTime.now().second}),
                                  child: Text('Go to #${color.hexString}'))))
                          .toList())),
            ])));
  }
}
