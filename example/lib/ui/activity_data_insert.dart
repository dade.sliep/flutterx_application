import 'package:example/localization/labels.dart';
import 'package:example/ui/activity_data_delegate.dart';
import 'package:example/ui/activity_home.dart';
import 'package:example/ui/view/data_insert.dart';
import 'package:example/ui/view_model/view_model_main.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class DataInsertActivity extends StatefulWidget {
  static final ActivityRoute<String?> route = ActivityRoute(
      title: (_) => 'Insert Data',
      location: '/insert_activity',
      systemUiStyle: (context) => SystemUiOverlayStyle(
          systemNavigationBarColor: Colors.red,
          systemNavigationBarIconBrightness: Brightness.light,
          statusBarColor: Colors.green.shade400,
          statusBarBrightness: Brightness.light,
          statusBarIconBrightness: Brightness.dark),
      builder: (context, args) => DataInsertActivity._(initialValue: args.getString('initialValue')));

  static Future<String?> open(BuildContext context, {String? initialValue}) =>
      route.open(context, args: {'initialValue': initialValue});

  final String initialValue;

  const DataInsertActivity._({required this.initialValue});

  @override
  State<DataInsertActivity> createState() => _DataInsertActivityState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('initialValue', initialValue));
  }
}

class _DataInsertActivityState extends State<DataInsertActivity> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.text = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = MainViewModel.of(context);
    return WillPopScope(
        child: Scaffold(
            appBar: AppBar(title: Text(labels.appName)),
            body: Center(
              child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                DataInsertWidget(controller: _controller),
                divider,
                LiveDataBuilder<bool>(
                    data: viewModel.canAccess,
                    builder: (context, value) => ElevatedButton(
                        onPressed: () => viewModel.canAccess.value = !value, child: Text('Can access: $value'))),
                ..._navigationDemo(context),
              ]),
            ),
            floatingActionButton: FloatingActionButton(
                onPressed: () => _controller.text.isEmpty ? null : Navigator.of(context).pop(_controller.text),
                child: const Icon(Icons.check))),
        onWillPop: () => Future.delayed(const Duration(seconds: 1), () => true));
  }

  Iterable<Widget> _navigationDemo(BuildContext context) sync* {
    yield Text(labels.navigation);
    yield const SizedBox(height: 8);
    yield Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Wrap(
            alignment: WrapAlignment.center,
            children: <Widget>[
              ElevatedButton(
                  onPressed: () => _controller.text.isEmpty
                      ? null
                      : DataDelegateActivity.openReplace(context,
                          message: 'Activity replaced\nResult has been returned', result: _controller.text),
                  child: Text(labels.openReplaceActivity)),
              ElevatedButton(
                  onPressed: () => DataDelegateActivity.openForward(context, message: 'This message will be returned'),
                  child: Text(labels.openForwardActivity)),
            ].interpolate(const SizedBox(width: 16)).toList()));
  }
}
