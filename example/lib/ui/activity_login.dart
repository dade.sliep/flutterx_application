import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide DialogRoute;
import 'package:flutterx_application/flutterx_application.dart';

class LoginActivity extends StatefulWidget {
  static final ActivityRoute<void> route = ActivityRoute(
      title: (_) => 'Login',
      location: '/login',
      builder: (context, args) => LoginActivity._(redirect: args['redirect']));

  final RouteSettings? redirect;

  const LoginActivity._({this.redirect});

  @override
  State<LoginActivity> createState() => _LoginActivityState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<RouteSettings?>('redirect', redirect));
  }
}

class _LoginActivityState extends State<LoginActivity> {
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text('Login')),
      body: Center(
        child: ElevatedButton(onPressed: _authorize, child: const Text('AUTHORIZE')),
      ));

  void _authorize() => LoginActivity.route.open(context);
}
