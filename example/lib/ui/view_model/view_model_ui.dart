import 'package:flutter/material.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_application/flutterx_ui.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';

class UIViewModel extends ViewModel with TaskViewModel {
  final LiveEvent<Message> messages = LiveEvent();

  UIViewModel._(super._provider) {
    messages.observeForever((message) => message.show(context), dispatcher: Dispatcher.endOfFrame);
  }

  factory UIViewModel.of(BuildContext context) => ViewModelProvider.of(context).provide(UIViewModel._);
}
