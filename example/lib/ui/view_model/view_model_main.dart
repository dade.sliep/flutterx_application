import 'dart:math';

import 'package:example/ui/view_model/view_model_ui.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_application/flutterx_ui.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_preferences/flutterx_preferences.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class MainViewModel extends ViewModel with TaskViewModel {
  final MutableLiveData<bool> canAccess =
      StorageMutableLiveData.bool(defaultValue: false, storage: sessionStorage, key: 'can_access');
  final MutableLiveData<UserInfo?> userInfo = MutableLiveData();
  final MutableLiveData<Object?> error = MutableLiveData();
  late final UIViewModel _ui = UIViewModel.of(context);

  MainViewModel._(super._provider);

  Future<void> loadUserInfo() => Future.delayed(
          const Duration(seconds: 1),
          () => Random().nextBool()
              ? throw StateError('random error')
              : userInfo.value =
                  const UserInfo(userId: 'george', username: 'george', name: 'George', surname: 'George'))
      .notify(this, args: {'level': 'critical'});

  @override
  Future<T> addTask<T>(Future<T> task, {JsonObject? args, Object? tag}) {
    error.value = null;
    return super.addTask(task, args: args, tag: tag);
  }

  @override
  void handleTaskSuccess(TaskWrapper task) {
    super.handleTaskSuccess(task);
    _ui.messages.send(const Message.success(title: 'YEah'));
  }

  @override
  void handleTaskError(TaskWrapper task, Object error, StackTrace stackTrace) {
    super.handleTaskError(task, error, stackTrace);
    _ui.messages.send(Message.error(title: 'ERRORE: $error'));
    if (task.args['level'] == 'critical') {
      userInfo.value = null;
      this.error.value = error;
    }
  }

  factory MainViewModel.of(BuildContext context) => ViewModelProvider.of(context).provide(MainViewModel._);
}

@immutable
class UserInfo {
  final String userId;
  final String username;
  final String name;
  final String surname;

  const UserInfo({required this.userId, required this.username, required this.name, required this.surname});
}
