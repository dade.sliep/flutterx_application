import 'package:collection/collection.dart';
import 'package:example/localization/labels.dart';
import 'package:example/ui/activity_data_insert.dart';
import 'package:example/ui/activity_fragment.dart';
import 'package:example/ui/bottom_sheet_data_insert.dart';
import 'package:example/ui/view_model/view_model_main.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide DialogRoute;
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_application/flutterx_ui.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class HomeActivity extends StatefulWidget {
  static final ActivityRoute<void> route = ActivityRoute(
      title: (args) => 'Home',
      location: '/home',
      queryParameters: const {'lang'},
      builder: (context, args) => HomeActivity._(lang: args['lang']));

  final String? lang;

  const HomeActivity._({required this.lang});

  @override
  State<HomeActivity> createState() => _HomeActivityState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('lang', lang));
  }
}

const Widget divider = Divider(indent: 32, endIndent: 32, height: 64);

class _HomeActivityState extends State<HomeActivity> {
  late final _viewModel = MainViewModel.of(context);
  int _counter = 0;
  String? _navResult;

  @override
  Widget build(BuildContext context) => Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text(labels.appName), flexibleSpace: AppBarLoader(loading: _viewModel.loading)),
      body: ResponsiveScreenContent(
        child: Card(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            //            Expanded(child: FlutterHtmlRenderer(src: HtmlSource.document("""
            //  <html style="height: 100%; width: 100%;">
            //    <body style="height: 100%; width: 100%; margin: 0px">
            //        <div style="height: 100%; width: 100%; background-color: blue;">
            //      </div>
            //    </body>
            //  </html>
            //  """))),
            Text(labels.contentText(_counter)),
            divider,
            ..._localizationDemo(context),
            divider,
            ..._navigationDemo(context),
            divider,
            LiveDataBuilder<UserInfo?>(
                data: _viewModel.userInfo, builder: (context, user) => Text('User: ${user?.userId}')),
            LiveDataBuilder<Object?>(data: _viewModel.error, builder: (context, error) => Text('Error: $error')),
            ElevatedButton(onPressed: () => _viewModel.loadUserInfo(), child: const Text('LOAD USER')),
            divider,
            ElevatedButton(
                onPressed: () => _dialogRoute.open(context, args: {}).then(_onNavResult), child: const Text('Dialog')),
          ]),
        ),
      ),
      floatingActionButton:
          FloatingActionButton(onPressed: () => setState(() => _counter++), child: const Icon(Icons.add)));

  Iterable<Widget> _localizationDemo(BuildContext context) sync* {
    final app = Application.of(context);
    final supportedLocales = app.locale.supportedLocales;
    final defaultLocale = app.locale.defaultLocale;
    final currentLocale = Localizations.localeOf(context);
    final page = AppPage.of(context);
    if (widget.lang != null && currentLocale.countryCode != widget.lang) {
      final l = supportedLocales.firstWhereOrNull((element) => element.countryCode == widget.lang);
      if (l == null)
        page.updateBrowserUri({'lang': null});
      else
        app.locale.enforcedLocale.value = l;
    }
    yield Text(MediaQuery.of(context).size.width.toString());
    yield Text(labels.chooseLanguage);
    yield const SizedBox(height: 8);
    yield ToggleButtons(
        isSelected: supportedLocales.map((locale) => locale == currentLocale).toList(),
        onPressed: (value) {
          final l = supportedLocales[value];
          app.locale.enforcedLocale.value = l;
          page.updateBrowserUri({'lang': l.countryCode});
        },
        textStyle: Theme.of(context).textTheme.titleLarge,
        borderRadius: BorderRadius.circular(m4),
        borderWidth: 1,
        borderColor: Colors.grey[600],
        selectedBorderColor: Colors.grey[900],
        children: supportedLocales
            .map((locale) => Text((locale == defaultLocale ? '*' : '') + locale.languageCode.toUpperCase()))
            .toList());
  }

  Iterable<Widget> _navigationDemo(BuildContext context) sync* {
    final viewModel = MyViewModel.of(context);
    yield Text(labels.navigation);
    yield const SizedBox(height: m8);
    yield LiveDataBuilder<String>(data: viewModel.data, builder: (context, value) => Text('DATA: $value'));
    yield const SizedBox(height: m8);
    yield Padding(
        padding: const EdgeInsets.symmetric(horizontal: m16),
        child: Wrap(
            alignment: WrapAlignment.center,
            children: <Widget>[
              ElevatedButton(
                  onPressed: () =>
                      DataInsertActivity.open(context, initialValue: _navResult ?? 'ciao').then(_onNavResult),
                  child: Text(labels.openActivity)),
              ElevatedButton(
                  onPressed: () => DataInsertBottomSheet.open(context, initialValue: 'ciao').then(_onNavResult),
                  child: Text(labels.openBottomSheet)),
              ElevatedButton(onPressed: () => FragmentActivity.route.open(context), child: const Text('Fragments')),
            ].interpolate(const SizedBox(width: m16)).toList()));
    yield const SizedBox(height: m8);
    yield Text(labels.navigationResult(_navResult.toString()));
  }

  static final DialogRoute<String?> _dialogRoute = DialogRoute(
      location: '/alert',
      builder: (context, args) {
        final key = GlobalKey();
        final viewModel = MyViewModel.of(context);
        return AlertDialog(
            actions: [
              AlertDialogAction.neutral(onAction: () => 'NEUTRAL PRESSED (text clicked: ${args['clicked'] == true})'),
              AlertDialogAction.negative(onAction: () => 'NEGATIVE PRESSED (text clicked: ${args['clicked'] == true})'),
              AlertDialogAction.positive(
                  buttonBuilder: AlertDialogAction.buttonElevated,
                  onAction: () => 'POSITIVE PRESSED (text clicked: ${args['clicked'] == true})'),
            ],
            content: MaterialStatefulWidget(onTap: () {
              args['clicked'] = true;
              viewModel.data.value = 'something!';
            }, builder: (context, states, child) {
              final content = Material(
                key: key,
                elevation: states.pressed || states.hovered || states.dragged ? 10 : 2,
                color: Colors.orange[100],
                child: SizedBox(
                  height: 100,
                  width: 240,
                  child: RichText(
                      text: TextSpan(
                          text: 'CLICK HERE',
                          style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              decoration: states.pressed || states.hovered || states.dragged
                                  ? TextDecoration.underline
                                  : null))),
                ),
              );
              return LongPressDraggable(
                  feedback: content,
                  childWhenDragging: const SizedBox(height: 100, width: 240),
                  onDragStarted: () => states.dragged = true,
                  onDragEnd: (_) => states.dragged = false,
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    LiveDataBuilder<String>(data: viewModel.data, builder: (context, value) => Text('DATA: $value')),
                    content,
                  ]));
            }));
      });

  void _onNavResult(String? result) => mounted ? setState(() => _navResult = result) : null;
}

class MyViewModel extends ViewModel {
  final MutableLiveData<String> data = MutableLiveData(value: 'nothing');

  MyViewModel._(super.provider);

  factory MyViewModel.of(BuildContext context) => ViewModelProvider.of(context).provide(MyViewModel._);
}
