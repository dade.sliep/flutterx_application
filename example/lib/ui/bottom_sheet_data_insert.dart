import 'package:example/localization/labels.dart';
import 'package:example/ui/activity_data_delegate.dart';
import 'package:example/ui/activity_home.dart';
import 'package:example/ui/view/data_insert.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class DataInsertBottomSheet extends StatefulWidget {
  static final BottomSheetRoute<String?> route = BottomSheetRoute.draggable(
      location: '/bottom_sheet',
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(24))),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (context, args) => DataInsertBottomSheet._(initialValue: args['initialValue']));

  static Future<String?> open(BuildContext context, {String? initialValue}) async =>
      route.open(context, args: {'initialValue': initialValue});

  final String initialValue;

  const DataInsertBottomSheet._({required this.initialValue});

  @override
  State<DataInsertBottomSheet> createState() => _DataInsertBottomSheetState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('initialValue', initialValue));
  }
}

class _DataInsertBottomSheetState extends State<DataInsertBottomSheet> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.text = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: Text(labels.appName)),
      body: Center(
          child: SingleChildScrollView(
              physics: const NeverScrollableScrollPhysics(),
              child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                DataInsertWidget(controller: _controller),
                divider,
                ..._navigationDemo(context),
              ]))),
      floatingActionButton: FloatingActionButton(
          onPressed: () => _controller.text.isEmpty ? null : Navigator.maybePop(context, _controller.text),
          child: const Icon(Icons.check)));

  Iterable<Widget> _navigationDemo(BuildContext context) sync* {
    yield Text(labels.navigation);
    yield const SizedBox(height: 8);
    yield Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Wrap(
            alignment: WrapAlignment.center,
            children: <Widget>[
              ElevatedButton(
                  onPressed: () => _controller.text.isEmpty
                      ? null
                      : DataDelegateActivity.openReplace(context,
                          message: 'Activity replaced\nResult has been returned', result: _controller.text),
                  child: Text(labels.openReplaceActivity)),
              ElevatedButton(
                  onPressed: () => DataDelegateActivity.openForward(context, message: 'This message will be returned'),
                  child: Text(labels.openForwardActivity)),
            ].interpolate(const SizedBox(width: 16)).toList()));
  }
}
