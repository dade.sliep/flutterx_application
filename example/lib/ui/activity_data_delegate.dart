import 'package:example/localization/labels.dart';
import 'package:example/ui/view_model/view_model_main.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_application/flutterx_application.dart';

class DataDelegateActivity extends StatefulWidget {
  static final ActivityRoute<String?> route = ActivityRoute(
      title: (_) => 'Delegate',
      location: '/delegate_activity',
      accessControl: (context, args) => MainViewModel.of(context).canAccess.value
          ? const RouteAccess.granted()
          : RouteAccess.denied(result: 'no_access', redirect: RouteInformation(uri: Uri.parse('/home'))),
      systemUiStyle: (context) => defaultSystemUiStyle(context).copyWith(statusBarIconBrightness: Brightness.dark),
      builder: (context, args) => DataDelegateActivity._(message: args['message']));

  static Future<String?> openReplace(BuildContext context, {required String message, Object? result}) =>
      route.openReplace(context, args: {'message': message}, result: result);

  static Future<void> openForward(BuildContext context, {required String message}) =>
      route.openForward(context, args: {'message': message}, mapResult: (result) => '$result (mapped)');

  final String message;

  const DataDelegateActivity._({required this.message});

  @override
  State<DataDelegateActivity> createState() => _DataDelegateActivityState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('message', message));
  }
}

class _DataDelegateActivityState extends State<DataDelegateActivity> {
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: Text(labels.appName)),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(widget.message),
          ElevatedButton(onPressed: () => Navigator.pop(context, widget.message), child: const Text('RETURN')),
        ]),
      ));
}
