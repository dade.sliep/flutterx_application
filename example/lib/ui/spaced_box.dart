import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class SpacedBox extends StatelessWidget {
  static const BorderSide _side = BorderSide(color: Colors.pinkAccent, width: .5);
  final double dimension;
  final Axis axis;

  const SpacedBox.horizontal({super.key, required this.dimension}) : axis = Axis.horizontal;

  const SpacedBox.vertical({super.key, required this.dimension}) : axis = Axis.vertical;

  @override
  Widget build(BuildContext context) => DecoratedBox(
      decoration: BoxDecoration(
          border: axis == Axis.horizontal
              ? const Border(bottom: _side, right: _side, left: _side)
              : const Border(left: _side, top: _side, bottom: _side)),
      child: SizedBox(
        width: axis == Axis.horizontal ? dimension : 12,
        height: axis == Axis.vertical ? dimension : 12,
        child: Center(
            child: Text('${dimension.truncate()}', style: const TextStyle(color: Colors.pinkAccent, fontSize: 8))),
      ));

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DoubleProperty('dimension', dimension))
      ..add(EnumProperty<Axis>('axis', axis));
  }
}
