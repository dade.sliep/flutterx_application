import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide DialogRoute;
import 'package:flutterx_application/flutterx_application.dart';

class PrivateActivity extends StatefulWidget {
  static final ActivityRoute<void> route =
      ActivityRoute(location: '/access', builder: (context, args) => PrivateActivity._(data: args['data']));

  final String data;

  const PrivateActivity._({required this.data});

  static Future<void> open(BuildContext context, {required String data}) => route.open(context, args: {'data': data});

  @override
  State<PrivateActivity> createState() => _PrivateActivityState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('data', data));
  }
}

class _PrivateActivityState extends State<PrivateActivity> {
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text('Private content')),
      body: Center(child: Text('Only authorized users can see this page!\ndata: ${widget.data}')));
}
