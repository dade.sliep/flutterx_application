import 'package:example/localization/labels.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DataInsertWidget extends StatelessWidget {
  final TextEditingController controller;

  const DataInsertWidget({super.key, required this.controller});

  @override
  Widget build(BuildContext context) => Padding(
      padding: const EdgeInsets.all(32),
      child: TextFormField(
          controller: controller,
          textInputAction: TextInputAction.newline,
          keyboardType: TextInputType.multiline,
          textCapitalization: TextCapitalization.sentences,
          maxLines: 4,
          maxLength: 1000,
          decoration: InputDecoration(
            hintText: labels.insertData,
            border: const OutlineInputBorder(),
            suffixIcon: IconButton(onPressed: controller.clear, icon: const Icon(Icons.clear)),
          )));

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<TextEditingController>('controller', controller));
  }
}
