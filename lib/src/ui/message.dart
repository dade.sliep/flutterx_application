import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

@immutable
class Message {
  final IconData icon;
  final Color color;
  final String title;
  final String? body;
  final Future? cancel;
  final List<MessageAction> actions;

  const Message({
    required this.icon,
    required this.color,
    required this.title,
    this.body,
    this.cancel,
    this.actions = const [],
  });

  const Message.success({required this.title, this.body, this.cancel, this.actions = const [MessageAction()]})
      : icon = Icons.check_circle_outline_outlined,
        color = const Color(0xff81c784);

  const Message.info({required this.title, this.body, this.cancel, this.actions = const [MessageAction()]})
      : icon = Icons.info_outlined,
        color = const Color(0xff4fc3f7);

  const Message.warning({required this.title, this.body, this.cancel, this.actions = const [MessageAction()]})
      : icon = Icons.warning_amber_outlined,
        color = const Color(0xffffb74d);

  const Message.error({required this.title, this.body, this.cancel, this.actions = const [MessageAction()]})
      : icon = Icons.error_outline_outlined,
        color = const Color(0xffd32f2f);

  Future<MaterialBannerClosedReason> show(BuildContext context) => Application.of(context).showMessage(this);

  MaterialBanner build(BuildContext context, {required ValueSetter<MaterialBannerClosedReason> hide}) {
    final foregroundColor = color.brightness.isDark ? Colors.white : Colors.black;
    final textTheme = Theme.of(context).textTheme;
    return MaterialBanner(
        backgroundColor: color,
        leading: Icon(icon, color: foregroundColor),
        content: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(title, style: textTheme.titleMedium!.copyWith(color: foregroundColor)),
          if (body != null) Text(body!, style: textTheme.bodySmall!.copyWith(color: foregroundColor.blend(color, .12))),
        ]),
        onVisible: () => (cancel ?? Future.delayed(const Duration(seconds: 4)))
            .whenComplete(() => hide(MaterialBannerClosedReason.hide)),
        actions: actions
            .map((action) => action._build(
                context, TextStyle(color: foregroundColor), () => hide(MaterialBannerClosedReason.dismiss)))
            .toList());
  }

  @override
  bool operator ==(Object other) =>
      other is Message &&
      icon == other.icon &&
      color == other.color &&
      title == other.title &&
      body == other.body &&
      cancel == other.cancel &&
      listEquals(actions, other.actions);

  @override
  int get hashCode => Object.hash(icon, color, title, body, cancel, Object.hashAll(actions));
}

@immutable
class MessageAction {
  final String? label;
  final ValueGetter<bool>? onTap;

  const MessageAction({this.label, this.onTap}) : assert(label == null || label.length > 0, 'label cannot be empty');

  Widget _build(BuildContext context, TextStyle style, VoidCallback hide) => OutlinedButton(
      onPressed: () => onTap?.call() ?? true ? hide() : null,
      child: Text(label ?? MaterialLocalizations.of(context).closeButtonLabel, style: style));

  @override
  bool operator ==(Object other) => other is MessageAction && label == other.label && onTap == other.onTap;

  @override
  int get hashCode => Object.hash(label, onTap);
}
