import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FooterView extends StatelessWidget {
  final Widget body;
  final Widget footer;
  final bool scrollable;

  const FooterView({
    super.key,
    required this.body,
    required this.footer,
    this.scrollable = true,
  });

  @override
  Widget build(BuildContext context) => scrollable
      ? CustomScrollView(primary: false, shrinkWrap: true, slivers: <Widget>[
          SliverList(delegate: SliverChildListDelegate([body])),
          SliverFillRemaining(
              hasScrollBody: false,
              child: Align(alignment: Alignment.bottomCenter, child: SizedBox(width: double.infinity, child: footer))),
        ])
      : Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [body, footer]);

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<bool>('scrollable', scrollable));
  }
}
