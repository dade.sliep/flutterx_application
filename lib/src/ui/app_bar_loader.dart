import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';

/// Use this inside flexibleSpace of [AppBar] widget
class AppBarLoader extends StatefulWidget {
  final LiveData<bool> loading;
  final LiveData<double?>? value;
  final Color? backgroundColor;
  final Color? color;
  final Animation<Color?>? valueColor;
  final double? minHeight;
  final String? semanticsLabel;
  final String? semanticsValue;

  const AppBarLoader({
    super.key,
    required this.loading,
    this.value,
    this.backgroundColor,
    this.color,
    this.valueColor,
    this.minHeight,
    this.semanticsLabel,
    this.semanticsValue,
  });

  AppBarLoader.value({
    super.key,
    required LiveData<double?> this.value,
    this.backgroundColor,
    this.color,
    this.valueColor,
    this.minHeight,
    this.semanticsLabel,
    this.semanticsValue,
  }) : loading = value.transform((value) => value != null);

  @override
  State<AppBarLoader> createState() => _AppBarLoaderState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<LiveData<bool>>('loading', loading))
      ..add(DiagnosticsProperty<LiveData<double?>?>('value', value))
      ..add(ColorProperty('backgroundColor', backgroundColor))
      ..add(ColorProperty('color', color))
      ..add(DiagnosticsProperty<Animation<Color?>?>('valueColor', valueColor))
      ..add(DoubleProperty('minHeight', minHeight))
      ..add(StringProperty('semanticsLabel', semanticsLabel))
      ..add(StringProperty('semanticsValue', semanticsValue));
  }
}

class _AppBarLoaderState extends State<AppBarLoader> with SingleTickerProviderStateMixin, ObserverMixin, StateObserver {
  late final AnimationController _controller = AnimationController(vsync: this, duration: kThemeAnimationDuration)
    ..addListener(_handleChange);

  @override
  void registerObservers() =>
      widget.loading.observe(this, (value) => value ? _controller.forward() : _controller.reverse());

  @override
  Widget build(BuildContext context) {
    if (_controller.value > 0) {
      final progressTheme = ProgressIndicatorTheme.of(context);
      final value = widget.value;
      final color =
          widget.valueColor?.value ?? widget.color ?? progressTheme.color ?? Theme.of(context).colorScheme.primary;
      final trackColor = widget.backgroundColor ?? progressTheme.linearTrackColor ?? color.withOpacity(.24);
      return Align(
          alignment: Alignment.bottomCenter,
          child: SizedBox(
              height: (progressTheme.linearMinHeight ?? 4) * _controller.value,
              child: value == null
                  ? _buildIndicator(null, trackColor, color)
                  : LiveDataBuilder<double?>(
                      data: value, builder: (context, value) => _buildIndicator(value, trackColor, color))));
    }
    return const SizedBox.shrink();
  }

  Widget _buildIndicator(double? value, Color trackColor, Color color) => LinearProgressIndicator(
      value: value,
      backgroundColor: trackColor,
      color: color,
      valueColor: widget.valueColor,
      minHeight: widget.minHeight,
      semanticsLabel: widget.semanticsLabel,
      semanticsValue: widget.semanticsValue);

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _handleChange() => setState(() {});
}
