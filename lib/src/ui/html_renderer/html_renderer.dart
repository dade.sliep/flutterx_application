import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

export 'html_renderer_stub.dart' if (dart.library.html) 'html_renderer_web.dart';

typedef HtmlRendererBuilder = Widget Function(BuildContext context, ValueChanged<Object> setupElement);

@immutable
class HtmlSource {
  final JsonObject headers;
  final bool isUri;
  final String src;

  const HtmlSource.document(this.src)
      : isUri = false,
        headers = const {};

  const HtmlSource.url(this.src, {this.headers = const {}}) : isUri = true;

  HtmlSource.uri(Uri uri, {this.headers = const {}})
      : isUri = true,
        src = uri.toString();

  @override
  bool operator ==(Object other) =>
      other is HtmlSource && isUri == other.isUri && src == other.src && mapEquals(headers, other.headers);

  @override
  int get hashCode => Object.hash(isUri, src, headers);
}
