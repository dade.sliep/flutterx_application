import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_application/src/ui/html_renderer/html_renderer.dart';

class FlutterHtmlRenderer extends StatelessWidget {
  final HtmlSource src;
  final ValueChanged<Object>? setupElement;

  const FlutterHtmlRenderer({super.key, required this.src, this.setupElement});

  @override
  Widget build(BuildContext context) => const SizedBox.shrink();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<HtmlSource>('src', src))
      ..add(ObjectFlagProperty<ValueChanged<Object>?>.has('setupElement', setupElement));
  }
}

class FlutterHtmlRendererContentSize extends StatelessWidget {
  final ValueChanged<Object>? setupElement;
  final HtmlRendererBuilder builder;

  const FlutterHtmlRendererContentSize({super.key, this.setupElement, required this.builder});

  @override
  Widget build(BuildContext context) => const SizedBox.shrink();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(ObjectFlagProperty<ValueChanged<Object>?>.has('setupElement', setupElement))
      ..add(ObjectFlagProperty<HtmlRendererBuilder>.has('builder', builder));
  }
}

class FlutterHtmlMessageListener extends StatelessWidget {
  final ValueChanged<Object> onMessage;
  final Widget? child;

  const FlutterHtmlMessageListener({super.key, required this.onMessage, this.child});

  @override
  Widget build(BuildContext context) => child ?? const SizedBox.shrink();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(ObjectFlagProperty<ValueChanged<Object>>.has('onMessage', onMessage));
  }
}

Future<void> importScript(HtmlSource script) => SynchronousFuture(null);
