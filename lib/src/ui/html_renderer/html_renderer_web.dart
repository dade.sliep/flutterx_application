import 'dart:async';
import 'dart:html' hide VoidCallback; // ignore: avoid_web_libraries_in_flutter
import 'dart:js_util' as js_util; // ignore: avoid_web_libraries_in_flutter
import 'dart:ui_web';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_application/src/ui/html_renderer/html_renderer.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';

class FlutterHtmlRenderer extends StatefulWidget {
  final HtmlSource src;
  final ValueChanged<Object>? setupElement;

  const FlutterHtmlRenderer({super.key, required this.src, this.setupElement});

  @override
  State<FlutterHtmlRenderer> createState() => _FlutterHtmlRendererState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<HtmlSource>('src', src))
      ..add(ObjectFlagProperty<ValueChanged<IFrameElement>?>.has('setupElement', setupElement));
  }
}

class _FlutterHtmlRendererState extends State<FlutterHtmlRenderer> {
  late final String _viewType = 'flutter-html-renderer-$hashCode';
  late final IFrameElement _element = IFrameElement()
    ..id = _viewType
    ..contentEditable = 'false'
    ..style.border = 'none'
    ..style.width = '100%'
    ..style.height = '100%';

  @override
  void initState() {
    super.initState();
    platformViewRegistry.registerViewFactory(_viewType, (viewId) {
      widget.setupElement?.call(_element);
      return _element..load(widget.src);
    });
  }

  @override
  void didUpdateWidget(FlutterHtmlRenderer oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.src != widget.src) _element.load(widget.src);
  }

  @override
  Widget build(BuildContext context) => HtmlElementView(viewType: _viewType);
}

class FlutterHtmlRendererContentSize extends StatefulWidget {
  final ValueChanged<Object>? setupElement;
  final HtmlRendererBuilder builder;

  const FlutterHtmlRendererContentSize({super.key, this.setupElement, required this.builder});

  @override
  State<FlutterHtmlRendererContentSize> createState() => _FlutterHtmlRendererContentSizeState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(ObjectFlagProperty<ValueChanged<Object>?>.has('setupElement', setupElement))
      ..add(ObjectFlagProperty<HtmlRendererBuilder>.has('builder', builder));
  }
}

class _FlutterHtmlRendererContentSizeState extends State<FlutterHtmlRendererContentSize> {
  static const HtmlSource _getContentSize = HtmlSource.document('''
  function getContentSize(iframeId) {
    var content = document.getElementById(iframeId).contentWindow.document.body.children[0];
    return content !== undefined ? { 'width': content.width === '100%' ? null : content.offsetWidth, 'height': content.width === '100%' ? null : content.offsetHeight } : null;
  }
  ''');
  Size _size = Size.infinite;
  bool _init = false;

  @override
  Widget build(BuildContext context) => SizedBox.fromSize(
      size: _size,
      child: Opacity(
          opacity: _init ? 1 : 0.01,
          child: widget.builder(context, (iframe) {
            importScript(_getContentSize);
            iframe as IFrameElement;
            iframe.onLoad.listen((event) {
              final values = js_util.dartify(js_util.callMethod(window, 'getContentSize', [iframe.id])) as Map?;
              if (values != null)
                Dispatcher.postFrame.post(() {
                  final size = Size(values['width'] ?? double.infinity, values['height'] ?? double.infinity);
                  if (size.width > 0 && size.height > 0)
                    setState(() {
                      _size = size;
                      _init = true;
                    });
                  else if (!_init) setState(() => _init = true);
                });
            });
            widget.setupElement?.call(iframe);
          })));
}

class FlutterHtmlMessageListener extends StatelessWidget {
  final ValueChanged<Object> onMessage;
  final Widget? child;

  const FlutterHtmlMessageListener({super.key, required this.onMessage, this.child});

  @override
  Widget build(BuildContext context) => child ?? const SizedBox.shrink();

  @override
  StatelessElement createElement() => _FlutterHtmlMessageListenerElement(this);

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(ObjectFlagProperty<ValueChanged<Object>>.has('onMessage', onMessage));
  }
}

class _FlutterHtmlMessageListenerElement extends StatelessElement {
  late final StreamSubscription _subscription;

  @override
  FlutterHtmlMessageListener get widget => super.widget as FlutterHtmlMessageListener;

  _FlutterHtmlMessageListenerElement(FlutterHtmlMessageListener super.widget);

  @override
  void mount(Element? parent, Object? newSlot) {
    _subscription = window.onMessage.listen(_onMessage);
    super.mount(parent, newSlot);
  }

  @override
  void unmount() {
    _subscription.cancel();
    super.unmount();
  }

  void _onMessage(MessageEvent event) => widget.onMessage(event);
}

extension _IFrameElementExt on IFrameElement {
  static const HtmlSource _generateUrl = HtmlSource.document('''
  async function generateUrl(url, headers) {
    const response = await fetch(url, { headers: headers });
    const blob = new Blob([await response.arrayBuffer()], { type: response.headers.get("content-type") });
    return URL.createObjectURL(blob);
  }
  ''');

  Future<void> load(HtmlSource src) async {
    if (src.isUri) {
      if (src.headers.isNotEmpty) {
        await importScript(_generateUrl);
        final response = js_util.callMethod(window, 'generateUrl', [src.src, js_util.jsify(src.headers)]);
        this.src = await js_util.promiseToFuture(response); // ignore: unsafe_html
      } else
        this.src = src.src; // ignore: unsafe_html
    } else {
      srcdoc = src.src; // ignore: unsafe_html
    }
  }
}

Future<void> importScript(HtmlSource script) {
  final head = querySelector('head')!;
  final children = head.children;
  for (final element in children)
    if (element is ScriptElement) if (script.isUri ? element.src.endsWith(script.src) : element.innerHtml == script.src)
      return SynchronousFuture(null);
  final scriptElement = ScriptElement()
    ..type = 'text/javascript'
    ..charset = 'utf-8'
    ..async = true;
  script.isUri ? scriptElement.src = script.src : scriptElement.innerHtml = script.src; // ignore: unsafe_html
  children.add(scriptElement);
  return WidgetsBinding.instance.endOfFrame;
}
