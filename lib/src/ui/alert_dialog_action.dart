import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide DialogRoute;
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

/// Handle [Result] of
typedef OnActionResult<T> = void Function(BuildContext context, T value);

/// Builder for [AlertDialogAction]
typedef ButtonStyleBuilder = Widget Function({
  required VoidCallback? onPressed,
  required bool autofocus,
  required Widget child,
});

/// Define standard AlertDialog actions
/// Each action will return a value to it's [OnActionResult] handler which by default is [popResult]
class AlertDialogAction<T> extends StatelessWidget {
  final ValueGetter<FutureOr<T>>? onAction;
  final T? value;
  final OnActionResult<T>? onResult;
  final bool autofocus;
  final LiveData<bool>? enabled;
  final ButtonStyleBuilder buttonBuilder;
  final WidgetBuilder contentBuilder;

  AlertDialogAction({
    super.key,
    this.onAction,
    this.value,
    this.onResult = popResult,
    this.autofocus = false,
    this.enabled,
    this.buttonBuilder = buttonText,
    Object? label,
    Widget? child,
    WidgetBuilder? builder,
  })  : assert(value is T || onAction != null, 'exactly one of [value, onAction] must be defines'),
        assert([builder != null || child != null || label != null].count(true) == 1,
            'exactly one of [builder, child, label] must be defined'),
        contentBuilder = builder ?? ((context) => child ?? Text(label.toString()));

  const AlertDialogAction.positive({
    super.key,
    this.onAction,
    this.value,
    this.onResult = popResult,
    this.autofocus = false,
    this.enabled,
    this.buttonBuilder = buttonText,
  })  : assert(value is T || onAction != null, 'exactly one of [value, onAction] must be defines'),
        contentBuilder = _buildPositive;

  const AlertDialogAction.negative({
    super.key,
    this.onAction,
    this.value,
    this.onResult = popResult,
    this.autofocus = false,
    this.enabled,
    this.buttonBuilder = buttonText,
  })  : assert(value is T || onAction != null, 'exactly one of [value, onAction] must be defines'),
        contentBuilder = _buildNegative;

  const AlertDialogAction.neutral({
    super.key,
    this.onAction,
    this.value,
    this.onResult = popResult,
    this.autofocus = false,
    this.enabled,
    this.buttonBuilder = buttonText,
  })  : assert(value is T || onAction != null, 'exactly one of [value, onAction] must be defines'),
        contentBuilder = _buildNeutral;

  @override
  Widget build(BuildContext context) =>
      enabled == null ? _buildButton(context, true) : LiveDataBuilder<bool>(data: enabled!, builder: _buildButton);

  Widget _buildButton(BuildContext context, bool enabled) => buttonBuilder(
      onPressed: enabled
          ? () async {
              final result = onAction == null ? value as T : await onAction!();
              onResult?.call(context, result); // ignore: use_build_context_synchronously
            }
          : null,
      autofocus: autofocus,
      child: contentBuilder(context));

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(ObjectFlagProperty<ValueGetter<FutureOr<T>>?>.has('onAction', onAction))
      ..add(DiagnosticsProperty<T?>('value', value))
      ..add(ObjectFlagProperty<OnActionResult<T>>.has('onResult', onResult))
      ..add(ObjectFlagProperty<WidgetBuilder>.has('contentBuilder', contentBuilder))
      ..add(DiagnosticsProperty<bool>('autofocus', autofocus))
      ..add(DiagnosticsProperty<LiveData<bool>?>('enabled', enabled))
      ..add(ObjectFlagProperty<ButtonStyleBuilder>.has('buttonBuilder', buttonBuilder));
  }

  /// In case [result] is success, call [Navigator.pop] with the result, else do nothing.
  static void popResult(BuildContext context, Object? result) => Navigator.of(context).pop(result);

  // ignore: avoid_positional_boolean_parameters
  static void popIfTrue(BuildContext context, bool result) => result ? Navigator.of(context).pop(result) : null;

  static Widget _buildPositive(BuildContext context) => Text(MaterialLocalizations.of(context).okButtonLabel);

  static Widget _buildNegative(BuildContext context) => Text(MaterialLocalizations.of(context).cancelButtonLabel);

  static Widget _buildNeutral(BuildContext context) => Text(MaterialLocalizations.of(context).closeButtonLabel);

  static Widget buttonText({required VoidCallback? onPressed, required bool autofocus, required Widget child}) =>
      TextButton(onPressed: onPressed, autofocus: autofocus, child: child);

  static Widget buttonOutlined({required VoidCallback? onPressed, required bool autofocus, required Widget child}) =>
      OutlinedButton(onPressed: onPressed, autofocus: autofocus, child: child);

  static Widget buttonElevated({required VoidCallback? onPressed, required bool autofocus, required Widget child}) =>
      ElevatedButton(onPressed: onPressed, autofocus: autofocus, child: child);
}
