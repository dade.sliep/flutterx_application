import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// Simple loading spinner
///
/// [size] defaults to 32
/// [valueColor] if null, defaults to [ProgressIndicator.valueColor]
/// [margin] of the widget
class SimpleLoader extends StatelessWidget {
  final double size;
  final double strokeWidth;
  final Color? backgroundColor;
  final Color? color;
  final Animation<Color?>? valueColor;
  final bool center;
  final EdgeInsetsGeometry? margin;

  const SimpleLoader({
    super.key,
    this.size = 32,
    double? strokeWidth,
    this.backgroundColor,
    this.color,
    this.valueColor,
    this.center = false,
    this.margin,
  }) : strokeWidth = strokeWidth ?? size / 10;

  @override
  Widget build(BuildContext context) {
    Widget content = SizedBox.square(
        dimension: size,
        child: Padding(
            padding: EdgeInsets.all(strokeWidth / 2),
            child: CircularProgressIndicator(
                strokeWidth: strokeWidth, backgroundColor: backgroundColor, color: color, valueColor: valueColor)));
    if (margin != null) content = Padding(padding: margin!, child: content);
    if (center) content = Center(child: content);
    return content;
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DoubleProperty('size', size))
      ..add(DoubleProperty('strokeWidth', strokeWidth))
      ..add(ColorProperty('backgroundColor', backgroundColor))
      ..add(ColorProperty('color', color))
      ..add(DiagnosticsProperty<Animation<Color?>?>('valueColor', valueColor))
      ..add(DiagnosticsProperty<bool>('center', center))
      ..add(DiagnosticsProperty<EdgeInsetsGeometry?>('margin', margin));
  }
}
