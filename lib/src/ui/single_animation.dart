import 'package:flutter/material.dart';

/// This class allows you to handle multiple animations using a single [AnimationController] this is useful when you
/// want your widget to have multiple animations without impacting on performance
/// Some parts of your widget may go forward, other reverse, and other may not change: this can be handled using by
/// defining keys through [setup] and [setupValue] methods and then you can access controller value from [operator[]]
/// Once everything is set up you can call [animate] that checks direction data and may start animations
class SingleAnimationController {
  final AnimationController _controller;
  final Map<String, _AnimationData> _data = {};

  Duration? get duration => _controller.duration;

  set duration(Duration? value) => _controller.duration = value;

  SingleAnimationController({Duration? duration, required VoidCallback update, required TickerProvider vsync})
      : _controller = AnimationController(vsync: vsync, duration: duration, value: 1)..addListener(update);

  double operator [](String key) {
    final data = _data[key];
    if (data == null) throw StateError('No data found with key $key');
    return data.value(_controller);
  }

  void setup({required String key, required ValueGetter<bool> forward}) =>
      setupValue<bool>(key: key, value: forward, forward: _computeDirection);

  static bool _computeDirection(bool forward) => forward;

  void setupValue<T>({
    required String key,
    required ValueGetter<T> value,
    required bool Function(T value) forward,
  }) {
    var oldValue = value();
    AnimationDirection computeDirection() {
      final newValue = value();
      return oldValue != newValue
          ? forward(oldValue = newValue)
              ? AnimationDirection.forward
              : AnimationDirection.reverse
          : AnimationDirection.unchanged;
    }

    _data[key] = _AnimationData(computeDirection, AnimationDirection.unchanged,
        forward(oldValue) ? _controller.upperBound : _controller.lowerBound);
  }

  Future<void> animate({Duration? duration, Curve curve = Curves.linear}) {
    assert(!_controller.isAnimating, 'cannot animate until current animation is complete');
    var update = false;
    for (final value in _data.values) update |= value.update();
    if (update) {
      _controller.value = _controller.lowerBound;
      return _controller.animateTo(_controller.upperBound, duration: duration, curve: curve);
    }
    return Future.value();
  }

  Future<void> _current = Future.value();

  Future<void> animateThen({Duration? duration, Curve curve = Curves.linear}) =>
      _current = _current.then((_) => animate(duration: duration, curve: curve));

  void dispose() => _controller.dispose();
}

enum AnimationDirection {
  unchanged,
  forward,
  reverse,
}

class _AnimationData {
  final AnimationDirection Function() _computeDirection;
  AnimationDirection _direction;
  double _lastValue;

  _AnimationData(this._computeDirection, this._direction, this._lastValue);

  double value(AnimationController controller) {
    switch (_direction) {
      case AnimationDirection.unchanged:
        return _lastValue;
      case AnimationDirection.forward:
        return _lastValue = controller.value;
      case AnimationDirection.reverse:
        return _lastValue = controller.upperBound - controller.value;
    }
  }

  bool update() => (_direction = _computeDirection()) != AnimationDirection.unchanged;
}
