import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterx_application/flutterx_application.dart';

/// Applies [ScreenLayout] to child
class ResponsiveScreenContent extends StatelessWidget {
  /// If true uses window layout which results in better performance.
  /// Only set it to false if this widget does not fill window width
  final bool primary;
  final AlignmentGeometry alignment;
  final Widget child;

  const ResponsiveScreenContent({
    super.key,
    this.primary = true,
    this.alignment = Alignment.topCenter,
    required this.child,
  });

  @override
  Widget build(BuildContext context) => primary
      ? _buildContent(ScreenLayout.of(context), alignment, child)
      : LayoutBuilder(
          builder: (context, constraints) => _buildContent(
              ScreenLayout.fromSize(Size(constraints.maxWidth, constraints.maxHeight)), alignment, child));

  static Widget _buildContent(ScreenLayout layout, AlignmentGeometry alignment, Widget child) => Align(
      alignment: alignment,
      child: ConstrainedBox(
          constraints: layout.content == null
              ? const BoxConstraints(minWidth: double.infinity)
              : BoxConstraints.tightFor(width: layout.content),
          child: Padding(
              padding: layout.margin != null ? EdgeInsets.symmetric(horizontal: layout.margin!) : EdgeInsets.zero,
              child: child)));

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<bool>('primary', primary))
      ..add(DiagnosticsProperty<AlignmentGeometry>('alignment', alignment));
  }
}
