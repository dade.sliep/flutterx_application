import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class DraggableBottomSheet extends StatefulWidget {
  final double initialChildSize;
  final double minChildSize;
  final double maxChildSize;
  final double minFocusableExtent;
  final double? contentHeight;
  final bool updateInsets;
  final Widget child;

  const DraggableBottomSheet({
    super.key,
    this.initialChildSize = .75,
    this.minChildSize = .25,
    this.maxChildSize = 1,
    this.minFocusableExtent = .5,
    this.contentHeight,
    this.updateInsets = true,
    required this.child,
  })  : assert(minChildSize >= 0.0, 'minChildSize cannot be negative'),
        assert(maxChildSize <= 1.0, 'maxChildSize cannot be > 1'),
        assert(minChildSize <= initialChildSize, 'minChildSize cannot be > initialChildSize'),
        assert(initialChildSize <= maxChildSize, 'initialChildSize cannot be > maxChildSize'),
        assert(contentHeight == null || contentHeight >= 0, 'contentHeight cannot be negative');

  static ScrollController controllerOf(BuildContext context) =>
      context.findRootAncestorStateOfType<_DraggableBottomSheetState>()!._controller;

  @override
  State<DraggableBottomSheet> createState() => _DraggableBottomSheetState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DoubleProperty('initialChildSize', initialChildSize))
      ..add(DoubleProperty('minChildSize', minChildSize))
      ..add(DoubleProperty('maxChildSize', maxChildSize))
      ..add(DoubleProperty('minFocusableExtent', minFocusableExtent))
      ..add(DiagnosticsProperty<bool>('updateInsets', updateInsets))
      ..add(DoubleProperty('contentHeight', contentHeight));
  }
}

class _DraggableBottomSheetState extends State<DraggableBottomSheet> {
  late ScrollController _controller;
  late DraggableScrollableNotification _notification = DraggableScrollableNotification(
      extent: widget.initialChildSize,
      minExtent: widget.minChildSize,
      maxExtent: widget.maxChildSize,
      initialExtent: widget.initialChildSize,
      context: context);

  @override
  Widget build(BuildContext context) {
    late final mediaQuery = MediaQuery.of(context);
    late final screen = RootMediaQuery.of(context);
    final focusScope = widget.minFocusableExtent > 0 ? FocusScope.of(context) : null;
    final isDesktop = kIsWeb && ScreenLayout.of(context).mode == LayoutMode.desktop;
    return LayoutBuilder(
        builder: (context, constraints) => NotificationListener<DraggableScrollableNotification>(
            onNotification: (notification) {
              _notification = notification;
              focusScope?.canRequestFocus = notification.extent > widget.minFocusableExtent;
              return false;
            },
            child: DraggableScrollableSheet(
                minChildSize: widget.minChildSize,
                initialChildSize: isDesktop ? widget.maxChildSize : widget.initialChildSize,
                maxChildSize: widget.maxChildSize,
                builder: (context, scrollController) {
                  final maxHeight = constraints.maxHeight * widget.maxChildSize;
                  final height = widget.contentHeight?.let((desired) => min(desired, maxHeight)) ?? maxHeight;
                  final content = SizedBox(height: height, child: widget.child);
                  return SingleChildScrollView(
                      controller: _controller = scrollController,
                      physics: const NoOverScrollPhysics(),
                      child: widget.updateInsets && !isDesktop
                          ? _computeInsets(height, mediaQuery, screen, content)
                          : content);
                })));
  }

  MediaQuery _computeInsets(double height, MediaQueryData mediaQuery, MediaQueryData screen, Widget content) {
    final extent = _notification.extent / widget.maxChildSize;
    final scroll = (1 - extent) * height;
    return MediaQuery(
        data: mediaQuery.copyWith(
            padding: mediaQuery.padding.copyWith(
                top: max(0, height - scroll - screen.size.height + screen.padding.top) + mediaQuery.padding.top),
            viewInsets: mediaQuery.viewInsets.copyWith(bottom: scroll + mediaQuery.viewInsets.bottom)),
        child: content);
  }
}
