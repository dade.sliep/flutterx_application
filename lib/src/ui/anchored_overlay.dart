import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_utils/flutterx_utils.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

typedef OverlayBuilder = Widget Function(BuildContext context, Rect anchor, Rect overlay);

class AnchoredOverlay extends StatefulWidget {
  final MutableLiveData<bool> show;
  final Duration hideDelay;
  final OverlayBuilder overlayBuilder;
  final Widget child;

  AnchoredOverlay({
    super.key,
    required this.show,
    this.hideDelay = Duration.zero,
    bool barrierDismissible = true,
    bool interceptPointer = true,
    required List<Widget> entries,
    required this.child,
  }) : overlayBuilder = ((context, anchor, overlay) {
          final content = Material(
              type: MaterialType.transparency,
              child: SizedBox.expand(
                child: Stack(alignment: Alignment.center, children: [
                  if (barrierDismissible)
                    Positioned.fill(
                        child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTapUp: (event) => anchor.contains(event.globalPosition) ? show.value = false : null,
                            onTapDown: (event) => anchor.contains(event.globalPosition) ? null : show.value = false)),
                  ...entries,
                ]),
              ));
          return AnchoredOverlayData._(
              anchor: anchor, overlay: overlay, child: interceptPointer ? PointerInterceptor(child: content) : content);
        });

  const AnchoredOverlay.builder({
    super.key,
    required this.show,
    this.hideDelay = Duration.zero,
    required this.overlayBuilder,
    required this.child,
  });

  static AnchoredOverlayData anchorOf(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<AnchoredOverlayData>() ??
      (throw StateError('anchorOf called outside overlay context'));

  @override
  State<AnchoredOverlay> createState() => _AnchorOverlayState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<MutableLiveData<bool>>('show', show))
      ..add(DiagnosticsProperty<Duration>('hideDelay', hideDelay))
      ..add(ObjectFlagProperty<OverlayBuilder>.has('overlayBuilder', overlayBuilder));
  }
}

class _AnchorOverlayState extends State<AnchoredOverlay> with ObserverMixin, StateObserver {
  late MutexRun? _runHide = widget.hideDelay > Duration.zero ? MutexRun.last(widget.hideDelay) : null;
  OverlayEntry? _entry;

  @override
  void registerObservers() =>
      widget.show.observe(this, (show) => show ? _show() : _runHide?.call(_hide, fallback: null) ?? _hide(),
          dispatcher: Dispatcher.postFrame);

  Future<void> _hide() async {
    if (WidgetsBinding.instance.schedulerPhase != SchedulerPhase.idle) await WidgetsBinding.instance.endOfFrame;
    _entry
      ?..remove()
      ..dispose();
    _entry = null;
  }

  void _show() {
    _runHide?.invalidate();
    if (_entry != null) return;
    Overlay.of(context).insert(_entry = OverlayEntry(builder: _buildOverlay));
  }

  @override
  void didUpdateWidget(AnchoredOverlay oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.hideDelay != oldWidget.hideDelay)
      _runHide = widget.hideDelay > Duration.zero ? MutexRun.last(widget.hideDelay) : null;
  }

  @override
  Widget build(BuildContext context) => widget.child;

  @override
  void dispose() {
    _hide();
    super.dispose();
  }

  Widget _buildOverlay(BuildContext overlayContext) {
    final box = context.findRenderObject() as RenderBox;
    final overlayBox = Overlay.of(overlayContext).context.findRenderObject() as RenderBox;
    return LayoutBuilder(builder: (_, constraints) {
      if (!box.attached) return const SizedBox.shrink();
      final anchor = Rect.fromPoints(box.localToGlobal(Offset.zero, ancestor: overlayBox),
          box.localToGlobal(box.size.bottomRight(Offset.zero), ancestor: overlayBox));
      return widget.overlayBuilder(overlayContext, anchor, Offset.zero & overlayBox.size);
    });
  }
}

class AnchoredOverlayData extends InheritedWidget {
  final Rect anchor;
  final Rect overlay;

  const AnchoredOverlayData._({required this.anchor, required this.overlay, required super.child});

  @override
  bool updateShouldNotify(AnchoredOverlayData oldWidget) => anchor != oldWidget.anchor || overlay != oldWidget.overlay;

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<Rect>('anchor', anchor))
      ..add(DiagnosticsProperty<Rect>('overlay', overlay));
  }
}
