import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// Apply [style] to both annotated region and [AppBarTheme]
class AnnotatedSystemUIStyle extends StatelessWidget {
  final SystemUiOverlayStyle style;
  final Widget child;

  const AnnotatedSystemUIStyle({super.key, required this.style, required this.child});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Theme(
        data: theme.copyWith(appBarTheme: theme.appBarTheme.copyWith(systemOverlayStyle: style)),
        child: AnnotatedRegion<SystemUiOverlayStyle>(value: style, child: child));
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<SystemUiOverlayStyle>('style', style));
  }
}
