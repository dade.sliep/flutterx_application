import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';

class ScopedData<T> extends StatefulWidget {
  final ValueGetter<T> factory;
  final LiveWidgetBuilder<T> builder;

  const ScopedData({super.key, required this.factory, required this.builder});

  @override
  State<ScopedData<T>> createState() => _ScopedDataState<T>();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(ObjectFlagProperty<ValueGetter<T>>.has('factory', factory))
      ..add(ObjectFlagProperty<LiveWidgetBuilder<T>>.has('builder', builder));
  }
}

class _ScopedDataState<T> extends State<ScopedData<T>> {
  late final T _data = widget.factory();

  @override
  Widget build(BuildContext context) => widget.builder(context, _data);
}
