import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_application/flutterx_ui.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';

part 'expandable_list_tile.dart';

typedef ExpandableItemBuilder<T> = Widget Function(BuildContext context, ExpansionParameters params, T item);

class ExpandableListView<T, K> extends StatelessWidget {
  final LiveMap<K, bool> expanded;
  final List<T> items;
  final K Function(T item) itemKey;
  final ScrollPhysics? physics;
  final bool shrinkWrap;
  final EdgeInsetsGeometry? padding;
  final ExpandableItemBuilder<T> itemBuilder;

  const ExpandableListView({
    super.key,
    required this.expanded,
    required this.items,
    required this.itemKey,
    this.physics,
    this.shrinkWrap = false,
    this.padding,
    required this.itemBuilder,
  });

  @override
  Widget build(BuildContext context) => ListView.builder(
      physics: physics,
      shrinkWrap: shrinkWrap,
      itemCount: items.length,
      padding: padding,
      itemBuilder: (context, index) => ExpandableListViewWrapper<T, K>(
          expanded: expanded, index: index, items: items, itemKey: itemKey, builder: itemBuilder));

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<LiveMap<K, bool>>('expanded', expanded))
      ..add(IterableProperty<T>('items', items))
      ..add(ObjectFlagProperty<K Function(T item)>.has('itemKey', itemKey))
      ..add(DiagnosticsProperty<ScrollPhysics?>('physics', physics))
      ..add(DiagnosticsProperty<bool>('shrinkWrap', shrinkWrap))
      ..add(DiagnosticsProperty<EdgeInsetsGeometry?>('padding', padding))
      ..add(ObjectFlagProperty<ExpandableItemBuilder<T>>.has('itemBuilder', itemBuilder));
  }
}

class ExpandableListViewWrapper<T, K> extends StatefulWidget {
  final LiveMap<K, bool> expanded;
  final int index;
  final List<T> items;
  final K Function(T item) itemKey;
  final ExpandableItemBuilder<T> builder;

  const ExpandableListViewWrapper({
    super.key,
    required this.expanded,
    required this.index,
    required this.items,
    required this.itemKey,
    required this.builder,
  });

  @override
  State<ExpandableListViewWrapper<T, K>> createState() => _ExpandableListViewWrapperState<T, K>();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<LiveMap<K, bool>>('expanded', expanded))
      ..add(IntProperty('index', index))
      ..add(IterableProperty<T>('items', items))
      ..add(ObjectFlagProperty<K Function(T item)>.has('itemKey', itemKey))
      ..add(ObjectFlagProperty<ExpandableItemBuilder<T>>.has('builder', builder));
  }
}

class _ExpandableListViewWrapperState<T, K> extends State<ExpandableListViewWrapper<T, K>>
    with ObserverMixin, StateObserver {
  late ExpansionParameters _params = _computeParameters(widget.expanded);

  @override
  void registerObservers() => widget.expanded.observe(this, _handleExpansionChange, dispatcher: Dispatcher.postFrame);

  @override
  void didUpdateWidget(ExpandableListViewWrapper<T, K> oldWidget) {
    super.didUpdateWidget(oldWidget);
    _handleExpansionChange(widget.expanded);
  }

  @override
  Widget build(BuildContext context) => widget.builder(context, _params, widget.items[widget.index]);

  void _handleExpansionChange(Map<K, bool> expanded) {
    final params = _computeParameters(expanded);
    if (params != _params) setState(() => _params = params);
  }

  ExpansionParameters _computeParameters(Map<K, bool> expanded) {
    final widget = this.widget;
    final items = widget.items;
    final index = widget.index;
    final itemKey = widget.itemKey;
    final prevKey = index > 0 ? itemKey(items[index - 1]) : null;
    final key = itemKey(items[index]);
    final nextKey = index < items.length - 1 ? itemKey(items[index + 1]) : null;
    return ExpansionParameters._(prevKey != null ? expanded[prevKey] ?? false : null, expanded[key] ?? false,
        nextKey != null ? expanded[nextKey] ?? false : null, (value) => widget.expanded[key] = value);
  }
}

@immutable
class ExpansionParameters {
  final bool? prevExpanded;
  final bool expanded;
  final bool? nextExpanded;
  final ValueSetter<bool> _setExpanded;

  set expanded(bool value) => _setExpanded(value);

  bool get _startSpace => prevExpanded != null && (expanded || prevExpanded!);

  bool get _endSpace => nextExpanded != null && (expanded || nextExpanded!);

  bool get _startRound => expanded || (prevExpanded ?? true);

  bool get _endRound => expanded || (nextExpanded ?? true);

  const ExpansionParameters._(
    this.prevExpanded,
    this.expanded,
    this.nextExpanded,
    this._setExpanded,
  );

  void toggle() => _setExpanded(!expanded);

  @override
  bool operator ==(Object other) =>
      other is ExpansionParameters &&
      other.prevExpanded == prevExpanded &&
      other.expanded == expanded &&
      other.nextExpanded == nextExpanded;

  @override
  int get hashCode => Object.hash(prevExpanded, expanded, nextExpanded);
}

class ExpandableListItem extends StatefulWidget {
  final ExpansionParameters params;
  final bool alwaysBuildBody;
  final double expandedMargin;
  final BorderSide cardSide;
  final Radius cardRadius;
  final Color? cardColor;
  final Color? cardShadowColor;
  final Color? cardSurfaceTintColor;
  final double? cardElevation;
  final Clip? cardClipBehavior;
  final Duration duration;
  final Widget header;
  final Widget body;

  const ExpandableListItem({
    super.key,
    required this.params,
    this.alwaysBuildBody = true,
    this.expandedMargin = 12,
    this.cardSide = BorderSide.none,
    this.cardRadius = const Radius.circular(4),
    this.cardColor,
    this.cardShadowColor,
    this.cardSurfaceTintColor,
    this.cardElevation,
    this.cardClipBehavior,
    this.duration = kThemeAnimationDuration,
    required this.header,
    required this.body,
  }) : assert(expandedMargin >= 0, 'margin cannot be negative!');

  @override
  State<StatefulWidget> createState() => _ExpandableListItemState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<ExpansionParameters>('params', params))
      ..add(DiagnosticsProperty<bool>('maintainState', alwaysBuildBody))
      ..add(DoubleProperty('expandedMargin', expandedMargin))
      ..add(DiagnosticsProperty<BorderSide>('cardSide', cardSide))
      ..add(DiagnosticsProperty<Radius>('cardRadius', cardRadius))
      ..add(ColorProperty('cardColor', cardColor))
      ..add(ColorProperty('cardShadowColor', cardShadowColor))
      ..add(ColorProperty('cardSurfaceTintColor', cardSurfaceTintColor))
      ..add(DoubleProperty('cardElevation', cardElevation))
      ..add(EnumProperty<Clip?>('cardClipBehavior', cardClipBehavior))
      ..add(DiagnosticsProperty<Duration>('duration', duration));
  }
}

class _ExpandableListItemState extends State<ExpandableListItem> with SingleTickerProviderStateMixin {
  late final SingleAnimationController _controller =
      SingleAnimationController(duration: widget.duration, update: _handleChange, vsync: this)
        ..setup(key: 'start', forward: () => widget.params._startSpace)
        ..setup(key: 'end', forward: () => widget.params._endSpace)
        ..setup(key: 'expanded', forward: () => widget.params.expanded);

  @override
  void didUpdateWidget(ExpandableListItem oldWidget) {
    super.didUpdateWidget(oldWidget);
    _controller
      ..duration = widget.duration
      ..animateThen(curve: Curves.fastOutSlowIn);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final item = widget;
    final params = item.params;
    final expandedMargin = item.expandedMargin / 2;
    final cardRadius = item.cardRadius;
    final expanded = _controller['expanded'];
    return Card(
        margin:
            EdgeInsets.only(top: expandedMargin * _controller['start'], bottom: expandedMargin * _controller['end']),
        color: item.cardColor,
        shadowColor: item.cardShadowColor,
        surfaceTintColor: item.cardSurfaceTintColor,
        elevation: item.cardElevation,
        clipBehavior: item.cardClipBehavior,
        shape: RoundedRectangleBorder(
            side: widget.cardSide,
            borderRadius: BorderRadius.vertical(
                top: params._startRound ? cardRadius : Radius.zero,
                bottom: params._endRound ? cardRadius : Radius.zero)),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
          item.header,
          Opacity(
              opacity: expanded,
              child: ClipRect(
                  child: Align(
                      alignment: AlignmentDirectional.centerStart,
                      heightFactor: expanded,
                      child: expanded > 0 || widget.alwaysBuildBody ? item.body : const SizedBox.shrink()))),
        ]));
  }

  void _handleChange() => setState(() {});
}

class ExpandableListIcon extends StatelessWidget {
  final ExpansionParameters params;
  final double splashRadius;
  final Duration duration;

  const ExpandableListIcon({
    super.key,
    required this.params,
    this.splashRadius = 24,
    this.duration = kThemeAnimationDuration,
  });

  @override
  Widget build(BuildContext context) => IconButton(
      splashRadius: splashRadius,
      onPressed: params.toggle,
      icon: AnimatedRotation(
          turns: params.expanded ? 1 / 2 : 0,
          curve: Curves.fastOutSlowIn,
          duration: duration,
          child: const Icon(Icons.keyboard_arrow_down)));

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<ExpansionParameters>('params', params))
      ..add(DoubleProperty('splashRadius', splashRadius))
      ..add(DiagnosticsProperty<Duration>('duration', duration));
  }
}
