import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class KeepAliveBuilder extends StatefulWidget {
  final bool wantKeepAlive;
  final WidgetBuilder builder;

  const KeepAliveBuilder({super.key, this.wantKeepAlive = true, required this.builder});

  @override
  State<KeepAliveBuilder> createState() => _KeepAliveBuilderState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<bool>('wantKeepAlive', wantKeepAlive))
      ..add(ObjectFlagProperty<WidgetBuilder>.has('builder', builder));
  }
}

class _KeepAliveBuilderState extends State<KeepAliveBuilder> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => widget.wantKeepAlive;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.builder(context);
  }
}
