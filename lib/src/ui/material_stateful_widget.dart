import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class MaterialStatefulWidget extends StatefulWidget {
  final FocusNode? focusNode;
  final MouseCursor mouseCursor;
  final MaterialStates? states;
  final GestureTapDownCallback? onTapDown;
  final GestureTapCallback? onTap;
  final GestureTapCancelCallback? onTapCancel;
  final GestureTapCallback? onDoubleTap;
  final GestureLongPressCallback? onLongPress;
  final HitTestBehavior behavior;
  final ValueWidgetBuilder<MaterialStates> builder;
  final Widget? child;

  const MaterialStatefulWidget({
    super.key,
    this.focusNode,
    this.mouseCursor = MaterialStateMouseCursor.clickable,
    this.states,
    this.onTapDown,
    this.onTap,
    this.onTapCancel,
    this.onDoubleTap,
    this.onLongPress,
    this.behavior = HitTestBehavior.opaque,
    required this.builder,
    this.child,
  });

  @override
  State<MaterialStatefulWidget> createState() => _MaterialStatefulWidgetState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<FocusNode?>('focusNode', focusNode))
      ..add(DiagnosticsProperty<MouseCursor>('mouseCursor', mouseCursor))
      ..add(DiagnosticsProperty<MaterialStates?>('states', states))
      ..add(ObjectFlagProperty<GestureTapDownCallback?>.has('onTapDown', onTapDown))
      ..add(ObjectFlagProperty<GestureTapCallback?>.has('onTap', onTap))
      ..add(ObjectFlagProperty<GestureTapCancelCallback?>.has('onTapCancel', onTapCancel))
      ..add(ObjectFlagProperty<GestureTapCallback?>.has('onDoubleTap', onDoubleTap))
      ..add(ObjectFlagProperty<GestureLongPressCallback?>.has('onLongPress', onLongPress))
      ..add(ObjectFlagProperty<HitTestBehavior?>.has('behavior', behavior))
      ..add(ObjectFlagProperty<ValueWidgetBuilder<MaterialStates>>.has('builder', builder));
  }
}

class _MaterialStatefulWidgetState extends State<MaterialStatefulWidget> {
  late MaterialStates _states;

  @override
  void initState() {
    super.initState();
    _states = widget.states ?? MaterialStates();
    _states._observers.add(_onStatesChanged);
  }

  @override
  void didUpdateWidget(MaterialStatefulWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    final newStates = widget.states;
    if (newStates != null && !identical(_states, newStates)) {
      _states._observers.remove(_onStatesChanged);
      _states = newStates;
      _states._observers.add(_onStatesChanged);
    }
  }

  @override
  void dispose() {
    _states._observers.remove(_onStatesChanged);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Focus(
      focusNode: widget.focusNode,
      canRequestFocus: !_states.disabled,
      onFocusChange: (value) => _states.focused = value,
      child: MouseRegion(
          cursor: MaterialStateProperty.resolveAs<MouseCursor>(widget.mouseCursor, _states._states),
          onEnter: (_) => _states.hovered = true,
          onExit: (_) => _states.hovered = false,
          child: GestureDetector(
              onTapDown: _states.disabled
                  ? null
                  : (e) {
                      _states.pressed = true;
                      widget.onTapDown?.call(e);
                    },
              onTap: _states.disabled
                  ? null
                  : () {
                      _states.pressed = false;
                      widget.onTap?.call();
                    },
              onTapCancel: _states.disabled
                  ? null
                  : () {
                      widget.onTapCancel?.call();
                      _states.pressed = false;
                    },
              onDoubleTap: _states.disabled ? null : widget.onDoubleTap,
              onLongPress: _states.disabled ? null : widget.onLongPress,
              behavior: widget.behavior,
              child: widget.builder(context, _states, widget.child))));

  void _onStatesChanged() => setState(() {});
}

/// Controller for [MaterialState] set used by [MaterialStatefulWidget]
/// You can update states by [update] function
@immutable
class MaterialStates {
  final Set<MaterialState> _states = {};
  final Set<VoidCallback> _observers = {};

  bool get hovered => _states.contains(MaterialState.hovered);

  set hovered(bool value) => update(MaterialState.hovered, value: value);

  bool get focused => _states.contains(MaterialState.focused);

  set focused(bool value) => update(MaterialState.focused, value: value);

  bool get pressed => _states.contains(MaterialState.pressed);

  set pressed(bool value) => update(MaterialState.pressed, value: value);

  bool get dragged => _states.contains(MaterialState.dragged);

  set dragged(bool value) => update(MaterialState.dragged, value: value);

  bool get selected => _states.contains(MaterialState.selected);

  set selected(bool value) => update(MaterialState.selected, value: value);

  bool get scrolledUnder => _states.contains(MaterialState.scrolledUnder);

  set scrolledUnder(bool value) => update(MaterialState.scrolledUnder, value: value);

  bool get disabled => _states.contains(MaterialState.disabled);

  set disabled(bool value) => update(MaterialState.disabled, value: value);

  bool get error => _states.contains(MaterialState.error);

  set error(bool value) => update(MaterialState.error, value: value);

  bool update(MaterialState state, {required bool value}) {
    final result = value ? _states.add(state) : _states.remove(state);
    if (result) for (final notify in _observers) notify();
    return result;
  }

  @override
  bool operator ==(Object other) => other is MaterialStates && setEquals(other._states, _states);

  @override
  int get hashCode => Object.hash(runtimeType, Object.hashAll(_states));

  @override
  String toString() => 'MaterialStates(${_states.join(', ')})';
}
