part of 'expandable_list_view.dart';

class ExpandableTile extends StatefulWidget {
  final BorderSide cardSide;
  final Radius cardRadius;
  final Color? cardColor;
  final Color? collapsedCardColor;
  final Color? cardShadowColor;
  final Color? cardSurfaceTintColor;
  final double? cardElevation;
  final Clip? cardClipBehavior;
  final Duration duration;
  final Widget header;
  final EdgeInsetsGeometry tilePadding;
  final Widget body;

  const ExpandableTile({
    super.key,
    this.cardSide = BorderSide.none,
    this.cardRadius = const Radius.circular(4),
    this.cardColor,
    this.collapsedCardColor,
    this.cardShadowColor,
    this.cardSurfaceTintColor,
    this.cardElevation,
    this.cardClipBehavior = Clip.antiAlias,
    this.duration = kThemeAnimationDuration,
    required this.header,
    this.tilePadding = const EdgeInsets.symmetric(horizontal: m16, vertical: m4),
    required this.body,
  });

  @override
  State<ExpandableTile> createState() => _ExpandableTileState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BorderSide>('cardSide', cardSide))
      ..add(DiagnosticsProperty<Radius>('cardRadius', cardRadius))
      ..add(ColorProperty('cardColor', cardColor))
      ..add(ColorProperty('collapsedCardColor', collapsedCardColor))
      ..add(ColorProperty('cardShadowColor', cardShadowColor))
      ..add(ColorProperty('cardSurfaceTintColor', cardSurfaceTintColor))
      ..add(DoubleProperty('cardElevation', cardElevation))
      ..add(EnumProperty<Clip?>('cardClipBehavior', cardClipBehavior))
      ..add(DiagnosticsProperty<Duration>('duration', duration))
      ..add(DiagnosticsProperty<EdgeInsetsGeometry>('tilePadding', tilePadding));
  }
}

class _ExpandableTileState extends State<ExpandableTile> {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    final params = ExpansionParameters._(null, _expanded, null, _setExpanded);
    return ExpandableListItem(
        params: params,
        expandedMargin: 0,
        cardSide: widget.cardSide,
        cardRadius: widget.cardRadius,
        cardColor: _expanded ? widget.cardColor : widget.collapsedCardColor,
        cardShadowColor: widget.cardShadowColor,
        cardSurfaceTintColor: widget.cardSurfaceTintColor,
        cardElevation: widget.cardElevation,
        cardClipBehavior: widget.cardClipBehavior,
        duration: widget.duration,
        header: InkWell(
            onTap: params.toggle,
            child: Padding(
                padding: widget.tilePadding,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [widget.header, ExpandableListIcon(params: params)]))),
        body: widget.body);
  }

  void _setExpanded(bool expanded) => setState(() => _expanded = expanded);
}
