import 'dart:async';
import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_application/flutterx_ui.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_preferences/flutterx_preferences.dart';
import 'package:flutterx_utils/flutterx_utils.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

part 'app_route_activity.dart';
part 'app_route_bottom_sheet.dart';
part 'app_route_dialog.dart';
part 'app_route_information_parser.dart';
part 'app_route_information_provider.dart';
part 'app_router_delegate.dart';

/// Arguments that can be passed to a route
typedef RouteArgs = Map<String, dynamic>;

/// Builder of a route
typedef RouteWidgetBuilder = Widget Function(BuildContext context, RouteArgs args);

/// Builder of a page title
typedef RouteTitleBuilder = String Function(RouteArgs args);

/// Builder of a page key
typedef RouteKeyBuilder = LocalKey? Function(RouteArgs args);

/// Condition for accessing route
typedef RouteAccessControl<T> = RouteAccess<T> Function(BuildContext context, RouteArgs args);

@immutable
class RouteAccess<T> {
  final bool _granted;
  final FutureOr<T>? _result;
  final RouteInformation? _redirect;

  const RouteAccess.granted()
      : _granted = true,
        _result = null,
        _redirect = null;

  const RouteAccess.denied({FutureOr<T>? result, RouteInformation? redirect})
      : _granted = false,
        _result = result,
        _redirect = redirect;

  void checkGrantedOrThrow() {
    if (!_granted) throw RouteAccessDeniedException<T>._(_result, _redirect);
  }

  Future<T>? getResultOrNull() => _granted ? null : RouteAccessDeniedException<T>._(_result, _redirect).result;
}

class RouteAccessDeniedException<T> implements Exception {
  final FutureOr<T>? _result;
  final RouteInformation? _redirect;

  Future<T> get result {
    final result = _result;
    if (result == null) return Future.error(this);
    return result is Future<T> ? result : Future.value(result);
  }

  const RouteAccessDeniedException._(this._result, this._redirect);
}

/// A route to a navigation UI component.
///
/// [location] path of this route
/// [T] the result type for this route
@immutable
abstract class AppRoute<T> {
  final String location;
  final T? defaultValue;
  final RouteAccessControl<T>? accessControl;
  final RouteWidgetBuilder builder;

  const AppRoute._({
    required this.location,
    required this.defaultValue,
    required this.accessControl,
    required this.builder,
  });

  Future<T> open(BuildContext context, {RouteArgs args = const {}});

  Future<T> openReplace(BuildContext context, {RouteArgs args = const {}, Object? result}) {
    Navigator.of(context).pop(result);
    return open(context, args: args);
  }

  Future<X> openForward<X>(BuildContext context, {RouteArgs args = const {}, X Function(T result)? mapResult}) async {
    assert(X == T || mapResult != null, 'Cannot forward result of type $X to type $T');
    final navigator = Navigator.of(context);
    final result = await open(context, args: args);
    final mapped = mapResult == null ? result as X : mapResult(result);
    navigator.pop(mapped);
    return mapped;
  }

  @override
  bool operator ==(Object other) => other is AppRoute && other.location == location;

  @override
  int get hashCode => Object.hash(runtimeType, location);

  @override
  String toString() => '${objectRuntimeType(this, 'AppRoute')}($location)';

  static String routeName(Object data) => data is String ? data : data.toString().snakeCase;

  static String routeLocation(Object data) => Navigator.defaultRouteName + routeName(data);

  T _getOrDefaultOrThrow(T? value) => value is T
      ? value
      : defaultValue ??
          (throw StateError(
              'route popped with null result, expected a result of type $T.\nWhen route returns a non-nullable type a defaultValue must be provided!'));
}
