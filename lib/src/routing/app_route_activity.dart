part of 'app_route.dart';

class ActivityRoute<T> extends AppRoute<T> {
  final RouteTitleBuilder? title;
  final String? restorationId;
  final Set<String> queryParameters;
  final bool maintainState;
  final bool fullscreenDialog;
  final SystemUiStyleBuilder? systemUiStyle;

  const ActivityRoute({
    this.title,
    this.restorationId,
    required super.location,
    this.queryParameters = const {},
    this.maintainState = true,
    this.fullscreenDialog = false,
    super.defaultValue,
    this.systemUiStyle,
    super.accessControl,
    required super.builder,
  }) : super._();

  @override
  Future<T> open(BuildContext context, {RouteArgs args = const {}}) =>
      AppRouterDelegate.of(context).push(this, args: args);

  @override
  Future<T> openReplace(BuildContext context, {RouteArgs args = const {}, Object? result}) =>
      AppRouterDelegate.of(context).pushReplace(this, args: args, result: result);

  @factory
  AppPage<T> _createPage(BuildContext context, {required RouteArgs args}) {
    accessControl?.call(context, args).checkGrantedOrThrow();
    return AppPage._(route: this, args: args);
  }

  @optionalTypeArgs
  static ActivityRoute<T> of<T>(BuildContext context) => AppPage.of<T>(context).route;

  static Uri parametrizedUri(ActivityRoute route, {required RouteArgs args}) {
    final parameters = Map<String, String?>.fromEntries(args.entries
        .where((entry) => entry.value != null && route.queryParameters.contains(entry.key))
        .map((entry) => MapEntry(entry.key, entry.value as String)));
    return Uri.parse(route.location)
        .replace(queryParameters: parameters.isEmpty ? null : parameters, fragment: args['#']);
  }
}

class AppPage<T> extends Page {
  final Completer<T> _result = Completer();
  final List<AppRouterDelegate?> _router = [null];
  final List<Uri?> _location = [null];
  final ActivityRoute<T> route;
  final MutableLiveData<FragmentKey?> fragment;

  Uri get location => _location[0] ??= ActivityRoute.parametrizedUri(route, args: arguments);

  @override
  RouteArgs get arguments => super.arguments as RouteArgs;

  AppPage._({required this.route, required RouteArgs args})
      : fragment = MutableLiveData(value: args.optString('#')?.let(FragmentKey.new)),
        super(
            key: ValueKey(route.location),
            name: route.title?.call(args),
            arguments: args,
            restorationId: route.restorationId) {
    fragment.observeForever((key) => updateBrowserUri({'#': key?.name}));
  }

  void updateBrowserUri([RouteArgs? params]) {
    final router = _router[0];
    if (router == null) return;
    assert(router.currentConfiguration!.route == route, 'You can only update current route parameters');
    final args = {...arguments, if (params != null) ...params};
    final uri = ActivityRoute.parametrizedUri(route, args: args);
    fragment.value = uri.hasFragment ? FragmentKey(Uri.decodeComponent(uri.fragment)) : null;
    if (uri == location) return;
    _location[0] = uri;
    scheduleMicrotask(() => router.provider.routerReportsNewRouteInformation(RouteInformation(uri: uri, state: args),
        type: RouteInformationReportingType.neglect));
  }

  @override
  Route<T> createRoute(BuildContext context) => MaterialPageRoute(
      settings: this,
      maintainState: route.maintainState,
      fullscreenDialog: route.fullscreenDialog,
      builder: (context) {
        _router[0] = AppRouterDelegate.of(context);
        final content = route.builder(context, arguments);
        final systemUiStyle = route.systemUiStyle?.call(context);
        return systemUiStyle == null ? content : AnnotatedSystemUIStyle(style: systemUiStyle, child: content);
      })
    ..popped.then((value) => _result.complete(route._getOrDefaultOrThrow(value)), onError: _result.completeError);

  @override
  String toString() => '${objectRuntimeType(this, 'AppPage')}($location)';

  @optionalTypeArgs
  static AppPage<T> of<T>(BuildContext context) => ModalRoute.of(context)?.settings as AppPage<T>;
}
