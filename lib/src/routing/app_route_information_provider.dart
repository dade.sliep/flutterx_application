part of 'app_route.dart';

class AppRouteInformationProvider extends PlatformRouteInformationProvider {
  final AppRouterDelegate _router;

  AppRouteInformationProvider._(this._router)
      : super(initialRouteInformation: RouteInformation(uri: Uri.parse(PlatformDispatcher.instance.defaultRouteName)));

  @override
  void routerReportsNewRouteInformation(
    RouteInformation routeInformation, {
    RouteInformationReportingType type = RouteInformationReportingType.none,
  }) {
    if (type == RouteInformationReportingType.none && value.uri.path == routeInformation.uri.path) return;
    super.routerReportsNewRouteInformation(routeInformation, type: type);
  }

  @override
  Future<bool> didPushRouteInformation(RouteInformation routeInformation) async =>
      await _router._navigateHistory(routeInformation) || await super.didPushRouteInformation(routeInformation);
}
