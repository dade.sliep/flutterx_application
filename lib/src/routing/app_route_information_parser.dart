part of 'app_route.dart';

typedef OnUnknownRoute = RouteInformation? Function(RouteInformation routeInformation);

class AppRouteInformationParser extends RouteInformationParser<AppPage> {
  final BuildContext _context;
  final Map<String, ActivityRoute> _routes;
  final String _initialLocation;
  final OnUnknownRoute _onUnknownRoute;
  final Storage? _storage;
  final String _stackKey;
  final String _stateKey;

  AppRouteInformationParser._(
      this._context, this._routes, this._initialLocation, this._onUnknownRoute, this._storage, String storageKey)
      : _stackKey = '_${storageKey}_stack',
        _stateKey = '_${storageKey}_state';

  @override
  Future<AppPage> parseRouteInformation(RouteInformation routeInformation) {
    final page = _parseRouteInformationInternal(routeInformation);
    if (page != null) return SynchronousFuture(page);
    final redirect = _onUnknownRoute(routeInformation);
    return redirect == null || redirect.uri == routeInformation.uri
        ? throw StateError('Unknown route ${routeInformation.uri}')
        : SynchronousFuture(
            _parseRouteInformationInternal(redirect) ?? (throw StateError('Unknown redirect route ${redirect.uri}')));
  }

  @override
  RouteInformation restoreRouteInformation(AppPage configuration) {
    final storage = _storage;
    if (storage != null) {
      final args = RouteArgs.from(configuration.arguments)
        ..removeWhere((key, value) => configuration.route.queryParameters.contains(key));
      if (args.isEmpty) {
        storage.remove(_routeStateKey(configuration.route));
      } else {
        storage[_routeStateKey(configuration.route)] = jsonEncode(args);
      }
    }
    final uri = ActivityRoute.parametrizedUri(configuration.route, args: configuration.arguments);
    return RouteInformation(uri: uri, state: configuration.arguments);
  }

  AppPage? _parseRouteInformationInternal(RouteInformation routeInformation) {
    final uri = _routeUri(routeInformation);
    final route = _routes[uri.path];
    if (route == null) return null;
    final args = <String, dynamic>{if (routeInformation.state is RouteArgs) ...routeInformation.state as RouteArgs};
    final storage = _storage;
    if (storage != null) {
      final data = storage[_routeStateKey(route)];
      if (data != null) args.addAll(jsonDecode(data));
    }
    uri.hasFragment ? args['#'] = Uri.decodeComponent(uri.fragment) : args.remove('#');
    args.addEntries(uri.queryParameters.entries.where((entry) => route.queryParameters.contains(entry.key)));
    try {
      return route._createPage(_context, args: args);
    } on RouteAccessDeniedException catch (e) {
      return e._redirect?.let(_parseRouteInformationInternal);
    }
  }

  List<String> _parseRouteStack() {
    final data = _storage?[_stackKey];
    if (data == null) return const [];
    return List<String>.from(jsonDecode(data));
  }

  void _restoreRouteStack(List<String> locations) {
    final storage = _storage;
    if (storage == null) return;
    locations.isEmpty ? storage.remove(_stackKey) : storage[_stackKey] = jsonEncode(locations);
  }

  String _routeStateKey(ActivityRoute route) => '${_stateKey}_${route.location}';

  void clearStorage() => _storage?.removeWhere((key, value) => key.startsWith(_stackKey) || key.startsWith(_stateKey));

  Uri _routeUri(RouteInformation routeInformation) {
    final uri = routeInformation.uri;
    return uri.path == Navigator.defaultRouteName ? uri.replace(path: _initialLocation) : uri;
  }
}
