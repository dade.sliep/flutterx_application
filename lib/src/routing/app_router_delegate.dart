part of 'app_route.dart';

// ignore: prefer_mixin
class AppRouterDelegate extends RouterDelegate<AppPage> with ChangeNotifier, PopNavigatorRouterDelegateMixin {
  List<AppPage>? _stackPlaceholder;
  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  final Set<NavigatorObserver> _routeObservers = {};
  final Map<String, ActivityRoute> _routes;
  late final AppRouteInformationProvider provider;
  late final AppRouteInformationParser parser;
  final List<AppPage> _stack = [];

  @override
  AppPage? get currentConfiguration => _stack.lastOrNull;

  AppRouterDelegate(BuildContext context, Iterable<ActivityRoute> routes, ActivityRoute initialRoute,
      OnUnknownRoute onUnknownRoute, Storage? storage, String storageKey)
      : _routes = Map.fromEntries(routes.map((route) => MapEntry(route.location, route))) {
    assert(_routes.containsKey(initialRoute.location), 'initialRoute is not declared in routes');
    provider = AppRouteInformationProvider._(this);
    parser = AppRouteInformationParser._(context, _routes, initialRoute.location, onUnknownRoute, storage, storageKey);
    if (storage != null) addListener(() => parser._restoreRouteStack([..._stack.map((page) => page.route.location)]));
  }

  @override
  Future<void> setInitialRoutePath(AppPage configuration) {
    _stack.clear();
    for (final location in parser._parseRouteStack()) {
      if (configuration.route.location == location) break;
      parser._parseRouteInformationInternal(RouteInformation(uri: Uri.parse(location)))?.let(_stack.add);
    }
    return super.setInitialRoutePath(configuration);
  }

  @override
  Future<void> setNewRoutePath(AppPage configuration) {
    final current = currentConfiguration;
    if (configuration.location != current?.location) {
      if (current != null && configuration.location.path == current.location.path)
        current.updateBrowserUri(configuration.arguments);
      else
        _stack.add(configuration);
    }
    return SynchronousFuture(null);
  }

  @override
  Widget build(BuildContext context) => Navigator(
      key: navigatorKey,
      observers: _routeObservers.toList(),
      pages: _stack.isEmpty
          ? (_stackPlaceholder ??= [
              ActivityRoute(location: '/_', builder: (context, args) => const SizedBox.shrink())
                  ._createPage(context, args: const {})
            ])
          : [..._stack],
      onPopPage: _onPopPage,
      restorationScopeId: 'app_nav');

  void addNavigatorObserver(NavigatorObserver observer) =>
      _routeObservers.add(observer) ? Dispatcher.postFrame.post(notifyListeners) : null;

  void removeNavigatorObserver(NavigatorObserver observer) =>
      _routeObservers.remove(observer) ? Dispatcher.postFrame.post(notifyListeners) : null;

  final Mutex _mutex = Mutex();

  Future<T> push<T>(ActivityRoute<T> route, {RouteArgs args = const {}}) async {
    final AppPage<T> page;
    try {
      page = route._createPage(navigatorKey.currentContext!, args: args);
    } on RouteAccessDeniedException<T> catch (e) {
      return e.result;
    }
    await _mutex.lock(() async {
      await setNewRoutePath(page);
      await WidgetsBinding.instance.endOfFrame;
      Router.navigate(navigatorKey.currentContext!, notifyListeners);
    });
    return page._result.future;
  }

  Future<T> pushReplace<T>(ActivityRoute<T> route, {RouteArgs args = const {}, Object? result}) async {
    if (_stack.isEmpty) return push(route, args: args);
    final AppPage<T> page;
    try {
      page = route._createPage(navigatorKey.currentContext!, args: args);
    } on RouteAccessDeniedException<T> catch (e) {
      return e.result;
    }
    await _mutex.lock(() async {
      if (WidgetsBinding.instance.schedulerPhase != SchedulerPhase.idle) await WidgetsBinding.instance.endOfFrame;
      navigatorKey.currentState!.pop(result);
      await setNewRoutePath(page);
      await WidgetsBinding.instance.endOfFrame;
      Router.neglect(navigatorKey.currentContext!, notifyListeners);
    });
    return page._result.future;
  }

  Future<bool> _navigateHistory(RouteInformation routeInformation) async {
    final uri = parser._routeUri(routeInformation);
    final index = _stack.lastIndexWhere((page) => page.location.path == uri.path);
    if (index == -1) return false;
    final navigator = navigatorKey.currentState!;
    for (var i = _stack.length - 1; i > index; i--) {
      navigator.popUntil((route) => route.settings is AppPage);
      if (!await navigator.maybePop()) break;
    }
    return _stack[index].location == uri;
  }

  bool _onPopPage(Route route, dynamic result) {
    final configuration = route.settings;
    if (configuration is! AppPage) return false;
    if (route.didPop(result)) {
      _stack.remove(configuration);
      _mutex.lock(() async {
        await WidgetsBinding.instance.endOfFrame;
        Router.neglect(navigatorKey.currentContext!, notifyListeners);
      });
      return true;
    }
    return false;
  }

  static AppRouterDelegate of(BuildContext context) => Router.of(context).routerDelegate as AppRouterDelegate;

  static ActivityRoute currentOf(BuildContext context) => AppRouterDelegate.of(context).currentConfiguration!.route;
}
