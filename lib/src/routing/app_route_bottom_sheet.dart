part of 'app_route.dart';

class BottomSheetRoute<T> extends AppRoute<T> {
  final Color? backgroundColor;
  final double? elevation;
  final ShapeBorder? shape;
  final Clip? clipBehavior;
  final BoxConstraints? constraints;
  final Color? barrierColor;
  final bool isScrollControlled;
  final bool useRootNavigator;
  final bool isDismissible;
  final bool enableDrag;
  final AnimationController? transitionAnimationController;
  final Offset? anchorPoint;
  final MutexRun? run;

  const BottomSheetRoute({
    required super.location,
    this.backgroundColor,
    this.elevation,
    this.shape,
    this.clipBehavior,
    this.constraints,
    this.barrierColor,
    this.isScrollControlled = false,
    this.useRootNavigator = false,
    this.isDismissible = true,
    this.enableDrag = true,
    this.transitionAnimationController,
    this.anchorPoint,
    this.run,
    super.defaultValue,
    super.accessControl,
    required super.builder,
  }) : super._();

  BottomSheetRoute.draggable({
    double initialChildSize = .75,
    double minChildSize = .25,
    double maxChildSize = 1,
    double minFocusableExtent = .5,
    double? contentHeight,
    bool updateInsets = true,
    required super.location,
    Color? backgroundColor,
    double? elevation,
    ShapeBorder? shape,
    Clip? clipBehavior,
    this.barrierColor,
    this.useRootNavigator = false,
    this.isDismissible = true,
    this.transitionAnimationController,
    this.anchorPoint,
    this.run,
    super.defaultValue,
    super.accessControl,
    required RouteWidgetBuilder builder,
  })  : backgroundColor = Colors.transparent,
        elevation = 0,
        shape = null,
        clipBehavior = Clip.none,
        constraints = null,
        isScrollControlled = true,
        enableDrag = true,
        super._(builder: (context, args) {
          final sheetTheme = Theme.of(context).bottomSheetTheme;
          return DraggableBottomSheet(
              initialChildSize: initialChildSize,
              minChildSize: minChildSize,
              maxChildSize: maxChildSize,
              minFocusableExtent: minFocusableExtent,
              contentHeight: contentHeight,
              updateInsets: updateInsets,
              child: Material(
                  color: backgroundColor ?? sheetTheme.backgroundColor,
                  elevation: elevation ?? sheetTheme.elevation ?? 0,
                  shape: shape ?? sheetTheme.shape,
                  clipBehavior: clipBehavior ?? sheetTheme.clipBehavior ?? Clip.none,
                  child: builder(context, args)));
        });

  @override
  Future<T> open(BuildContext context, {RouteArgs args = const {}}) =>
      run != null ? run!.call<T>(() => _open(context, args)) : _open(context, args);

  Future<T> _open(BuildContext context, RouteArgs args) =>
      accessControl?.call(context, args).getResultOrNull() ??
      showModalBottomSheet<T>(
          context: context,
          backgroundColor: backgroundColor,
          elevation: elevation,
          shape: shape,
          clipBehavior: clipBehavior,
          constraints: constraints,
          barrierColor: barrierColor,
          isScrollControlled: isScrollControlled,
          useRootNavigator: useRootNavigator,
          isDismissible: isDismissible,
          enableDrag: enableDrag,
          routeSettings: RouteSettings(name: location, arguments: args),
          transitionAnimationController: transitionAnimationController,
          anchorPoint: anchorPoint,
          builder: (context) => PointerInterceptor(child: builder(context, args))).then(_getOrDefaultOrThrow);
}
