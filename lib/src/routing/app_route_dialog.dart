part of 'app_route.dart';

class DialogRoute<T> extends AppRoute<T> {
  final bool barrierDismissible;
  final Color? barrierColor;
  final String? barrierLabel;
  final bool useSafeArea;
  final bool useRootNavigator;
  final Offset? anchorPoint;
  final MutexRun? run;

  const DialogRoute({
    required super.location,
    this.barrierDismissible = true,
    this.barrierColor = Colors.black54,
    this.barrierLabel,
    this.useSafeArea = true,
    this.useRootNavigator = true,
    this.anchorPoint,
    this.run,
    super.defaultValue,
    super.accessControl,
    required super.builder,
  }) : super._();

  @override
  Future<T> open(BuildContext context, {RouteArgs args = const {}}) =>
      run != null ? run!.call<T>(() => _open(context, args)) : _open(context, args);

  Future<T> _open(BuildContext context, RouteArgs args) =>
      accessControl?.call(context, args).getResultOrNull() ??
      showDialog<T>(
          context: context,
          barrierDismissible: barrierDismissible,
          barrierColor: barrierColor,
          barrierLabel: barrierLabel,
          useSafeArea: useSafeArea,
          useRootNavigator: useRootNavigator,
          routeSettings: RouteSettings(name: location, arguments: args),
          anchorPoint: anchorPoint,
          builder: (context) => PointerInterceptor(child: builder(context, args))).then(_getOrDefaultOrThrow);
}
