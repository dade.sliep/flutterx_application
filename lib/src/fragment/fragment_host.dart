part of 'fragment.dart';

class FragmentHost extends StatefulWidget {
  final FragmentManager? manager;
  final AxisDirection axisDirection;
  final bool primary;
  final Storage? storage;
  final FragmentEntry? initialFragment;
  final UnknownEntryStrategy unknownEntryStrategy;
  final FragmentWrapper fragmentWrapper;
  final List<Fragment> fragments;

  @override
  FragmentHostKey get key => super.key! as FragmentHostKey;

  const FragmentHost({
    required FragmentHostKey super.key,
    this.manager,
    this.axisDirection = AxisDirection.right,
    this.primary = true,
    this.storage,
    this.initialFragment,
    this.unknownEntryStrategy = unknownEntryStrategyIdle,
    this.fragmentWrapper = removePrimaryScrollController,
    required this.fragments,
  }) : assert(fragments.length > 0, 'fragments cannot be empty');

  @override
  State<FragmentHost> createState() => FragmentHostState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<FragmentManager?>('manager', manager))
      ..add(EnumProperty<AxisDirection>('axisDirection', axisDirection))
      ..add(DiagnosticsProperty<bool>('primary', primary))
      ..add(DiagnosticsProperty<Storage?>('storage', storage))
      ..add(DiagnosticsProperty<FragmentEntry?>('initialFragment', initialFragment))
      ..add(ObjectFlagProperty<UnknownEntryStrategy>.has('unknownEntryStrategy', unknownEntryStrategy))
      ..add(ObjectFlagProperty<FragmentWrapper>.has('fragmentWrapper', fragmentWrapper));
  }

  static Widget removePrimaryScrollController(Fragment fragment) => PrimaryScrollController.none(child: fragment);

  static FragmentEntry? unknownEntryStrategyInitial(FragmentManager manager, FragmentEntry entry) =>
      manager.host.initialFragment ?? FragmentEntry(key: manager.host.fragments.first.key);

  static FragmentEntry? unknownEntryStrategyCrash(FragmentManager manager, FragmentEntry entry) => throw StateError(
      'Invalid entry: $entry, available entries: [${manager.host.fragments.map((fragment) => fragment.key).join(',')}]');

  static FragmentEntry? unknownEntryStrategyIdle(FragmentManager manager, FragmentEntry entry) => null;
}

class FragmentHostState extends State<FragmentHost> with _FragmentViewport {
  late final FragmentManager _defaultManager = FragmentManager();
  late FragmentManager _manager = widget.manager ?? _defaultManager;
  AppPage? _page;
  ScrollDirection _direction = ScrollDirection.idle;

  @override
  void initState() {
    super.initState();
    _manager._attach(this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _updatePageFragment();
    if (!_manager.currentFragment.initialized) _manager.currentFragment.value = _initialEntry();
  }

  @override
  void didUpdateWidget(FragmentHost oldWidget) {
    final managerChanged = oldWidget.manager != widget.manager;
    if (managerChanged) _manager._detach();
    super.didUpdateWidget(oldWidget);
    if (managerChanged)
      _manager = (widget.manager ?? _defaultManager).._attach(this);
    else if (oldWidget.storage != widget.storage)
      _manager.currentFragment._setup(key: widget.key, storage: widget.storage);
    if (oldWidget.primary != widget.primary) _updatePageFragment();
  }

  void _updatePageFragment() {
    final current = _page;
    final page = widget.primary ? AppPage.of(context) : null;
    if (current == page) return;
    if (current != null) current.fragment.removeObserver(_syncPrimaryFragment);
    _page = page;
    if (page != null) {
      if (_manager.currentFragment.initialized) page.fragment.value = _manager.currentFragment.value.key;
      page.fragment.observeForever(_syncPrimaryFragment);
    }
  }

  @override
  void dispose() {
    _page?.fragment.removeObserver(_syncPrimaryFragment);
    _manager._detach();
    super.dispose();
  }

  @override
  void _onFragmentIndexComplete() {
    if (_manager.currentFragment.initialized) _manager.currentFragment.invalidate();
  }

  Future<void> _pushEntry(FragmentEntry entry) {
    if (_reverting) return SynchronousFuture(null);
    return _beginTransition(entry, _direction);
  }

  _StackImage? _image;

  @override
  void _rollbackTransition() {
    _direction = ScrollDirection.idle;
    final image = _image;
    if (image != null)
      try {
        _reverting = true;
        _manager.currentFragment._restoreImage(image);
      } finally {
        _reverting = false;
      }
  }

  @override
  void _onFragmentTransitionComplete(FragmentEntry entry) {
    _direction = ScrollDirection.idle;
    _syncPrimaryFragmentUri(entry);
    _image = _manager.currentFragment._createImage();
  }

  bool _reverting = false;

  @override
  void _handleUnknownEntry(FragmentEntry entry, ScrollDirection direction) {
    try {
      _reverting = true;
      _manager.currentFragment.historyDelete();
    } finally {
      _reverting = false;
    }
    final redirect = widget.unknownEntryStrategy(_manager, entry);
    if (redirect != null) {
      if (!_indexByKey.containsKey(redirect.key)) throw StateError('Unknown entry loop');
      _manager.currentFragment.value = redirect;
    } else if (_manager.currentFragment.queue.isEmpty) {
      _manager.currentFragment.value = _initialEntry();
    } else {
      _manager.currentFragment.invalidate();
    }
  }

  bool _syncPage = false;

  void _syncPrimaryFragmentUri(FragmentEntry entry) {
    final page = _page;
    if (page != null)
      try {
        _syncPage = true;
        page.fragment.value = entry.key;
      } finally {
        _syncPage = false;
      }
  }

  void _syncPrimaryFragment(FragmentKey? key) {
    if (_syncPage || key == null) return;
    _manager.currentFragment._setEntryFromStack(key);
  }

  FragmentEntry _initialEntry() {
    final key = _page?.fragment.value;
    if (key != null && _indexByKey.containsKey(key)) return FragmentEntry(key: key);
    final entry = widget.initialFragment;
    if (entry != null && _indexByKey.containsKey(entry.key)) return entry;
    return FragmentEntry(key: _fragments.first.key);
  }
}

typedef UnknownEntryStrategy = FragmentEntry? Function(FragmentManager manager, FragmentEntry entry);
typedef FragmentWrapper = Widget Function(Fragment fragment);
