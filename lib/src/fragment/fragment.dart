import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_preferences/flutterx_preferences.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

part 'fragment_entry.dart';
part 'fragment_host.dart';
part 'fragment_key.dart';
part 'fragment_manager.dart';
part 'fragment_observer.dart';
part 'fragment_stack.dart';
part 'fragment_viewport.dart';
part 'fragment_will_pop_scope.dart';

abstract class Fragment extends StatefulWidget {
  @override
  FragmentKey get key => super.key! as FragmentKey;

  const Fragment({required FragmentKey super.key});

  @override
  FragmentState<Fragment> createState();

  static FragmentState of(BuildContext context) {
    final state = context.findAncestorStateOfType<FragmentState>();
    assert(state != null, 'Fragment.of(context) can only be used inside fragments');
    return state!;
  }
}

abstract class FragmentState<T extends Fragment> extends State<T> {
  late final FragmentHostState _container = requireNotNull(context.findAncestorStateOfType<FragmentHostState>(),
      message: () => 'Fragment must belong to a FragmentHost');
  final List<WillPopCallback> _willPopCallbacks = [];

  FragmentManager get manager => _container._manager;

  bool _active = false;

  bool get active => _active;

  RouteArgs? _args;

  @override
  void initState() {
    super.initState();
    _container._onFragmentAttached(this);
  }

  @override
  void activate() {
    super.activate();
    _container._onFragmentAttached(this);
  }

  @override
  void deactivate() {
    _container._onFragmentDetached(this);
    super.deactivate();
  }

  @override
  void dispose() {
    _container._onFragmentDetached(this);
    super.dispose();
  }

  @mustCallSuper
  void onResume() {
    assert(!_active, 'onResume called on active fragment');
    _active = true;
  }

  @mustCallSuper
  void onPause() {
    assert(_active, 'onPause called on non active fragment');
    _active = false;
  }

  X args<X>([Function? fromJson]) => fromJson == null ? _args as X : fromJson(_args); // ignore: avoid_dynamic_calls

  void setArgs(RouteArgs args) => setState(() => _args = args);

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<FragmentManager>('manager', manager))
      ..add(DiagnosticsProperty<bool>('active', active));
  }
}
