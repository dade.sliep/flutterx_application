part of 'fragment.dart';

class FragmentWillPopScope extends StatefulWidget {
  final WillPopCallback? onWillPop;
  final Widget child;

  const FragmentWillPopScope({super.key, required this.onWillPop, required this.child});

  @override
  State<FragmentWillPopScope> createState() => _FragmentWillPopScopeState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(ObjectFlagProperty<WillPopCallback?>.has('onWillPop', onWillPop));
  }
}

class _FragmentWillPopScopeState extends State<FragmentWillPopScope> {
  FragmentState? _fragment;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.onWillPop != null) _fragment?._willPopCallbacks.remove(widget.onWillPop);
    _fragment = context.findAncestorStateOfType<FragmentState>();
    if (widget.onWillPop != null) _fragment?._willPopCallbacks.add(widget.onWillPop!);
  }

  @override
  void didUpdateWidget(FragmentWillPopScope oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.onWillPop != oldWidget.onWillPop && _fragment != null) {
      if (oldWidget.onWillPop != null) _fragment!._willPopCallbacks.remove(oldWidget.onWillPop);
      if (widget.onWillPop != null) _fragment!._willPopCallbacks.add(widget.onWillPop!);
    }
  }

  @override
  void dispose() {
    if (widget.onWillPop != null) _fragment?._willPopCallbacks.remove(widget.onWillPop);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => widget.child;
}
