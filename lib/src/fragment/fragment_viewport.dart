part of 'fragment.dart';

mixin _FragmentViewport on State<FragmentHost> {
  final Mutex _run = Mutex();
  PageController? _controller;
  List<Fragment> _fragments = const [];
  List<Widget> _children = const [];
  Map<FragmentKey, int> _indexByKey = const {};

  @override
  void initState() {
    super.initState();
    _indexFragments();
  }

  @override
  void didUpdateWidget(FragmentHost oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (!listEquals(_fragments, widget.fragments)) _indexFragments();
  }

  @override
  Widget build(BuildContext context) {
    final controller = _controller;
    return controller == null
        ? const SizedBox()
        : Scrollable(
            key: ValueKey(widget.key),
            axisDirection: widget.axisDirection,
            controller: controller,
            physics: const NeverScrollableScrollPhysics(),
            restorationId: widget.key.name,
            scrollBehavior: ScrollConfiguration.of(context).copyWith(scrollbars: false),
            viewportBuilder: (context, position) =>
                Viewport(cacheExtent: 0, axisDirection: widget.axisDirection, offset: position, slivers: <Widget>[
                  SliverFillViewport(
                      viewportFraction: controller.viewportFraction, delegate: SliverChildListDelegate(_children)),
                ]));
  }

  Future<void> _indexFragments() => _run.lock(() {
        final fragments = _fragments = [...widget.fragments];
        _children = KeyedSubtree.ensureUniqueKeysForList([...fragments.map(widget.fragmentWrapper)]);
        _indexByKey = {for (var i = 0; i < fragments.length; i++) fragments[i].key: i};
        _onFragmentIndexComplete();
        return SynchronousFuture(null);
      });

  void _onFragmentIndexComplete();

  FragmentEntry? _pendingEntry;

  Future<void> _beginTransition(FragmentEntry entry, ScrollDirection direction) => _run.lock(() async {
        final requestedKey = entry.key;
        final requestedIndex = _indexByKey[requestedKey];
        if (requestedIndex == null) return _handleUnknownEntry(entry, direction);
        _pendingEntry = entry;
        final controller = _controller;
        if (controller == null) {
          setState(() => _controller = PageController(initialPage: requestedIndex, keepPage: false));
          await SchedulerBinding.instance.endOfFrame;
          _onFragmentTransitionComplete(entry);
          return;
        }
        if (controller.positions.isEmpty) await SchedulerBinding.instance.endOfFrame;
        final lastIndex = controller.page?.round() ?? requestedIndex;
        if (requestedIndex == lastIndex) return requestedKey.currentState!.setArgs(entry.args);
        final currentFragment = _fragments[lastIndex].key.currentState;
        if (currentFragment != null) {
          for (final callback in [...currentFragment._willPopCallbacks])
            if (!await callback()) return _rollbackTransition();
          _updateActiveState(currentFragment, false);
        }
        requestedKey.currentState?.let((state) {
          state.setArgs(entry.args);
          _updateActiveState(state, true);
        });
        final bool forward;
        switch (direction) {
          case ScrollDirection.idle:
            forward = lastIndex < requestedIndex;
            break;
          case ScrollDirection.forward:
            forward = true;
            break;
          case ScrollDirection.reverse:
            forward = false;
            break;
        }
        var startIndex = forward ? requestedIndex - 1 : requestedIndex + 1;
        if (startIndex == lastIndex) {
          await controller.animateToPage(requestedIndex, duration: kThemeAnimationDuration, curve: Curves.ease);
        } else {
          final originalChildren = _children;
          var endIndex = requestedIndex;
          setState(() {
            _children = [..._children];
            void swapIndices(int a, int b) {
              final tmp = _children[a];
              _children[a] = _children[b];
              _children[b] = tmp;
            }

            if (startIndex < 0) {
              startIndex = 0;
              endIndex = 1;
              if (lastIndex != endIndex) swapIndices(requestedIndex, endIndex);
            } else if (startIndex == _children.length) {
              startIndex--;
              endIndex = startIndex - 1;
              if (lastIndex != endIndex) swapIndices(requestedIndex, endIndex);
            }
            swapIndices(lastIndex, startIndex);
          });
          controller.jumpToPage(startIndex);
          await controller.animateToPage(endIndex, duration: kThemeAnimationDuration, curve: Curves.ease);
          if (!mounted) return;
          setState(() => _children = originalChildren);
          if (requestedIndex != endIndex) controller.jumpToPage(requestedIndex);
        }
        _onFragmentTransitionComplete(entry);
      });

  void _handleUnknownEntry(FragmentEntry entry, ScrollDirection direction) =>
      throw StateError('Requested transition but no fragment found with key ${entry.key}');

  void _rollbackTransition();

  void _onFragmentTransitionComplete(FragmentEntry entry);

  void _onFragmentAttached(FragmentState state) {
    final entry = _pendingEntry;
    if (entry != null && entry.key == state.widget.key) {
      state.setArgs(entry.args);
      _updateActiveState(state, true);
    }
  }

  void _onFragmentDetached(FragmentState state) {
    if (_pendingEntry?.key == state.widget.key) _updateActiveState(state, false);
  }

  static void _updateActiveState(FragmentState state, bool active) {
    if (state._active == active) return;
    if (active) {
      state.onResume();
      assert(state._active, 'super.onResume not called');
    } else {
      state.onPause();
      assert(!state._active, 'super.onResume not called');
    }
  }
}
