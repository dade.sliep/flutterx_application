part of 'fragment.dart';

/// Represents current fragments stack which can be
class FragmentStack extends MutableLiveData<FragmentEntry> with HistoryMixin<FragmentEntry> {
  late Queue<String> _queue;
  ValueSetter<int>? _setHistoryIndex;

  @override
  Queue<String> get queue => _queue;

  @override
  set index(int value) {
    _setHistoryIndex?.call(value);
    super.index = value;
  }

  @override
  String Function(FragmentEntry data)? get encode => _encodeFragmentEntry;

  @override
  FragmentEntry Function(String source)? get decode => _decodeFragmentEntry;

  FragmentStack._() : super.late();

  void _setup({required FragmentHostKey key, required Storage? storage}) {
    if (storage != null) {
      _queue = StorageLiveQueue(key: '${key.name}_fragment_stack', storage: storage);
      final indexKey = '${key.name}_fragment_stack_index';
      _setHistoryIndex = (value) => storage[indexKey] = value.toString();
      super.index = storage[indexKey]?.let(int.parse) ?? -1;
    } else {
      _queue = Queue();
      _setHistoryIndex = null;
      super.index = -1;
    }
  }

  void _setEntryFromStack(FragmentKey key) {
    if (valueOrNull?.key == key) return;
    var index = 0;
    for (final entry in _queue.map(_decodeFragmentEntry)) {
      if (entry.key == key) {
        this.index = index;
        return;
      }
      index++;
    }
    value = FragmentEntry(key: key);
  }

  _StackImage _createImage() => _StackImage(jsonEncode(_queue.toList()), index);

  void _restoreImage(_StackImage image) {
    final queue = Queue<String>.from(jsonDecode(image.queue));
    if (_queue is StorageLiveQueue)
      (_queue as StorageLiveQueue).setTo(queue);
    else
      _queue = queue;
    index = image.index;
  }

  static String _encodeFragmentEntry(FragmentEntry data) => jsonEncode(data.toJson());

  static FragmentEntry _decodeFragmentEntry(String data) => FragmentEntry.fromJson(jsonDecode(data));
}

class _StackImage {
  final String queue;
  final int index;

  _StackImage(this.queue, this.index);
}
