part of 'fragment.dart';

class FragmentManager {
  FragmentHostState? _hostState;

  FragmentHost get host => _hostState!.widget;

  final FragmentStack currentFragment = FragmentStack._();

  LiveData<bool> get canGoBack => currentFragment.canUndo;

  LiveData<bool> get canGoForward => currentFragment.canRedo;

  void navigateTo<T extends Fragment>({
    ScrollDirection direction = ScrollDirection.idle,
    RouteArgs args = const {},
    bool replace = false,
  }) {
    final key = _hostState!._fragments.whereType<T>().firstOrNull?.key;
    if (key == null) throw StateError('No fragment found with type $T');
    return navigateToKey(key, direction: direction, args: args, replace: replace);
  }

  void navigateToKey(
    FragmentKey key, {
    ScrollDirection direction = ScrollDirection.idle,
    RouteArgs args = const {},
    bool replace = false,
  }) {
    _hostState!._direction = direction;
    if (replace)
      currentFragment.historyReplace(FragmentEntry(key: key, args: args));
    else
      currentFragment.value = FragmentEntry(key: key, args: args);
  }

  bool back({ScrollDirection direction = ScrollDirection.reverse}) {
    if (currentFragment.canUndo.value) {
      _hostState!._direction = direction;
      currentFragment.historyUndo();
      return true;
    }
    return false;
  }

  bool forward({ScrollDirection direction = ScrollDirection.forward}) {
    if (currentFragment.canRedo.value) {
      _hostState!._direction = direction;
      currentFragment.historyRedo();
      return true;
    }
    return false;
  }

  void _attach(FragmentHostState state) {
    assert(_hostState == null, 'Host state already attached');
    _hostState = state;
    currentFragment
      .._setup(key: state.widget.key, storage: state.widget.storage)
      ..observeForever(state._pushEntry, dispatcher: Dispatcher.postFrame);
  }

  void _detach() {
    assert(_hostState != null, 'Host state not attached');
    currentFragment.removeObserver(_hostState!._pushEntry);
    _hostState = null;
  }

  static FragmentManager of(BuildContext context) {
    final state = context.findAncestorStateOfType<FragmentHostState>();
    assert(state != null, 'FragmentManager.of(context) can only be used inside fragments');
    return state!._manager;
  }
}
