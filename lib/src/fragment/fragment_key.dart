part of 'fragment.dart';

class FragmentHostKey extends GlobalKey<FragmentHostState> {
  final String name;

  const FragmentHostKey(this.name) : super.constructor();

  FragmentManager get manager =>
      currentState?._manager ?? (throw StateError('$this is not attached to any FragmentHost'));

  @override
  bool operator ==(Object other) => other is FragmentHostKey && other.name == name;

  @override
  int get hashCode => Object.hash(name, runtimeType);

  @override
  String toString() => 'FragmentHostKey($name)';
}

@immutable
class FragmentKey<T extends FragmentState> extends GlobalKey<T> {
  final String name;

  const FragmentKey(this.name) : super.constructor();

  @override
  bool operator ==(Object other) => other is FragmentKey && other.name == name;

  @override
  int get hashCode => Object.hash(name, runtimeType);

  @override
  String toString() => 'FragmentKey($name)';
}
