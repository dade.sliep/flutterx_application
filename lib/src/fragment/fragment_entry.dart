part of 'fragment.dart';

@immutable
class FragmentEntry with DTO {
  final FragmentKey key;
  final RouteArgs args;

  const FragmentEntry({
    required this.key,
    this.args = const {},
  });

  factory FragmentEntry.fromJson(JsonObject json) =>
      FragmentEntry(key: FragmentKey(json['name']), args: json['args'] ?? const {});

  @override
  JsonObject toJson() => {'name': key.name, if (args.isNotEmpty) 'args': args};

  @override
  bool operator ==(Object other) => other is FragmentEntry && other.key == key && other.args == args;

  @override
  int get hashCode => Object.hash(key, args);
}
