part of 'fragment.dart';

mixin FragmentStateObserver<T extends Fragment> on FragmentState<T>, ObserverMixin {
  @override
  void onResume() {
    super.onResume();
    doRegister();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_active) doRegister();
  }

  @override
  void didUpdateWidget(T oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (_active) doRegister();
  }

  @override
  void onPause() {
    doUnregister();
    super.onPause();
  }
}
