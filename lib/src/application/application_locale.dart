part of 'application.dart';

/// Define locale interface like this:
///
/// localization/labels.dart
///     part 'labels_en.dart';
///     part 'labels_it.dart';
///
///     late Labels labels;
///
///     class Labels extends LabelInterface {
///       static final Set<Labels> supportedLabels = {_it, _en};
///       final String helloWorld;
///
///       const Labels._({
///         required super.locale,
///         required this.helloWorld,
///       });
///
///       static void onLabel(Labels value) => labels = value;
///     }
/// localization/labels_en.dart
///     final _en = Labels._(
///       locale: const Locale('en', 'US'),
///       helloWorld: 'Hello World!',
///     );
/// localization/labels_it.dart
///     final _en = Labels._(
///       locale: const Locale('en', 'US'),
///       helloWorld: 'Ciao Mondo!',
///     );
/// Then you should link your interface to the application like this:
/// main.dart
///     void main() => runApplication(
///       ...
///         locale: LocaleData<Labels>(labels: Labels.supportedLabels, onLabel: Labels.onLabel),
///       ...
///     );
/// Now you are ready to access label static instance from everywhere in your project without worrying about context
/// dependency like this:
///     Text(labels.helloWorld)
/// Note: if you notice that your [StatefulWidget] does not rebuild after changing app locale you should probably
/// integrate this function call in your build method: [LocaleDataExt]
class LocaleData<T extends LabelInterface> {
  static const List<LocalizationsDelegate> _defaultLocalizationDelegates = [
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
  ];
  final List<Locale> supportedLocales;
  final MutableLiveData<Locale?> enforcedLocale;
  final List<LocalizationsDelegate> _localizationDelegates;

  late final Locale defaultLocale = basicLocaleListResolution(PlatformDispatcher.instance.locales, supportedLocales);

  LocaleData({
    required Set<T> labels,
    required ValueChanged<T> onLabel,
    Storage? storage,
    String storageKey = 'locale',
    List<LocalizationsDelegate> localizationDelegates = _defaultLocalizationDelegates,
  })  : supportedLocales = labels.map((label) => label.locale).toList(growable: false),
        enforcedLocale = _localeData(storage, storageKey),
        _localizationDelegates = [LabelInterfaceDelegate<T>(labels, onLabel), ...localizationDelegates];

  LocaleData.only({
    this.supportedLocales = const <Locale>[Locale('en', 'US')],
    Storage? storage,
    String storageKey = 'locale',
    List<LocalizationsDelegate> localizationDelegates = _defaultLocalizationDelegates,
  })  : enforcedLocale = _localeData(storage, storageKey),
        _localizationDelegates = localizationDelegates;

  static MutableLiveData<Locale?> _localeData(Storage? storage, String storageKey) => storage == null
      ? MutableLiveData()
      : StorageMutableLiveData(
          key: storageKey,
          storage: storage,
          encode: (value) => value == null ? '' : '${value.languageCode}_${value.countryCode}',
          decode: (source) {
            final code = source.split('_');
            return code.length == 2 ? Locale(code[0], code[1]) : null;
          });
}

/// You can call [useLabels] in your build method to ensure labels used in your [StatefulWidget] will be updated
/// on locale change
extension LocaleDataExt on BuildContext {
  void useLabels() => Localizations.localeOf(this);
}
