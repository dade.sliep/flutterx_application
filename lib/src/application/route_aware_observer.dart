part of 'application.dart';

mixin RouteAwareMixin<T extends StatefulWidget> on State<T> implements RouteAware {
  final RouteObserver<PageRoute> _routeObserver = RouteObserver();
  late AppRouterDelegate _router;
  bool _ready = false;
  bool _isOnTop = false;

  bool get isOnTop => _isOnTop;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _router = Application.of(context).router;
    final route = ModalRoute.of(context)! as PageRoute;
    _ready = route.settings == _router.currentConfiguration;
    _routeObserver.subscribe(this, route);
    _router.addNavigatorObserver(_routeObserver);
    _ready = true;
  }

  @override
  void dispose() {
    _routeObserver.unsubscribe(this);
    _router.removeNavigatorObserver(_routeObserver);
    super.dispose();
  }

  @override
  void didPopNext() => didChangeRouteState(isOnTop: true);

  @override
  void didPush() => didChangeRouteState(isOnTop: true);

  @override
  void didPop() => didChangeRouteState(isOnTop: false);

  @override
  void didPushNext() => didChangeRouteState(isOnTop: false);

  @protected
  @mustCallSuper
  void didChangeRouteState({required bool isOnTop}) {
    if (_ready) _isOnTop = isOnTop;
  }
}

mixin RouteAwareObserver<T extends StatefulWidget> on RouteAwareMixin<T>, ObserverMixin {
  @override
  void dispose() {
    if (_isOnTop) doUnregister();
    super.dispose();
  }

  @override
  void didChangeRouteState({required bool isOnTop}) {
    super.didChangeRouteState(isOnTop: isOnTop);
    isOnTop ? doRegister() : doUnregister();
  }
}
