part of 'application.dart';

class _AppBindingObserver extends WidgetsBindingObserver {
  final ApplicationState _app;

  _AppBindingObserver(this._app);

  @override
  void didChangeMetrics() => _app._updateAppScreen();

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) => (_app.lifecycle as MutableLiveData).value = state;

  void init() {
    didChangeAppLifecycleState(WidgetsBinding.instance.lifecycleState ?? AppLifecycleState.detached);
    WidgetsBinding.instance.addObserver(this);
  }

  void dispose() => WidgetsBinding.instance.removeObserver(this);
}
