import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_web_plugins/url_strategy.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_application/src/ui/message.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_preferences/flutterx_preferences.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

part 'application_binding.dart';
part 'application_impl.dart';
part 'application_locale.dart';
part 'application_screen.dart';
part 'route_aware_observer.dart';

/// Run application
///
/// This should be the application entrypoint
/// [name] of the application, cannot use a label here
/// [initialize] here you can initialize your dependencies and do initialization task and return the first route
/// [routes] a set of all routes of this app
/// [onUnknownRoute] called when push event is called by the system navigator but the route cannot be resolved
/// [locale] localization data
/// [appFactory] generate app widget
Future<void> runApplication({
  required String name,
  UrlStrategy? urlStrategy,
  InitializeApp? initialize,
  required Set<ActivityRoute> routes,
  OnUnknownRoute onUnknownRoute = _defaultOnUnknownRoute,
  required ThemeData theme,
  ValueGetter<LocaleData> locale = LocaleData.only,
  AppFactory appFactory = _defaultAppFactory,
}) async {
  setUrlStrategy(urlStrategy ?? FullPathUrlStrategy());
  WidgetsFlutterBinding.ensureInitialized();
  await Preferences.initialize();
  return Application._runApp(
      name: name,
      initialize: initialize,
      routes: routes,
      onUnknownRoute: onUnknownRoute,
      theme: theme,
      locale: locale(),
      appFactory: appFactory);
}

/// Factory for [SystemUiOverlayStyle] used by routes to define overlay style per page
typedef SystemUiStyleBuilder = SystemUiOverlayStyle Function(BuildContext context);

/// Default overlay style from appBarTheme and scaffoldBackgroundColor
SystemUiOverlayStyle defaultSystemUiStyle(BuildContext context) {
  final theme = Theme.of(context);
  final appBarTheme = theme.appBarTheme;
  final statusBarBrightness = appBarTheme.systemOverlayStyle?.statusBarBrightness ??
      appBarTheme.backgroundColor?.brightness ??
      theme.primaryColor.brightness;
  final navBarColor = theme.scaffoldBackgroundColor.brighten(-.05);
  return SystemUiOverlayStyle(
      systemNavigationBarColor: navBarColor,
      systemNavigationBarIconBrightness: navBarColor.brightness.negative,
      statusBarColor: Colors.black26,
      statusBarBrightness: statusBarBrightness,
      statusBarIconBrightness: appBarTheme.systemOverlayStyle?.statusBarIconBrightness ?? statusBarBrightness.negative);
}

RouteInformation? _defaultOnUnknownRoute(RouteInformation request) => null;

Widget _defaultAppFactory({
  required AppRouteInformationProvider routeInformationProvider,
  required AppRouteInformationParser routeInformationParser,
  required AppRouterDelegate routerDelegate,
  required GenerateAppTitle onGenerateTitle,
  required ThemeData theme,
  required TransitionBuilder builder,
  required Locale? locale,
  required Iterable<LocalizationsDelegate>? localizationsDelegates,
  required Iterable<Locale> supportedLocales,
}) =>
    MaterialApp.router(
        routeInformationProvider: routeInformationProvider,
        routeInformationParser: routeInformationParser,
        routerDelegate: routerDelegate,
        onGenerateTitle: onGenerateTitle,
        theme: theme,
        builder: builder,
        locale: locale,
        localizationsDelegates: localizationsDelegates,
        supportedLocales: supportedLocales,
        debugShowCheckedModeBanner: false);
