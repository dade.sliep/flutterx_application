part of 'application.dart';

/// Follow Material Guidelines to understand how to use this data class
/// https://material.io/design/layout/responsive-layout-grid.html
/// https://material.io/archive/guidelines/layout/responsive-ui.html
@immutable
class ScreenLayout {
  /// Guess screen device from it's size
  final LayoutMode mode;

  /// Number of columns of the screen
  final int columns;

  /// Horizontal content margin
  final double? margin;

  /// Content max width
  final double? content;

  const ScreenLayout({
    required this.mode,
    required this.columns,
    required this.margin,
    required this.content,
  }) : assert((margin == null) != (content == null), 'exactly one of margin or content parameter should be provided');

  factory ScreenLayout.fromSize(Size size) {
    final width = size.width;
    if (width < 600) {
      return const ScreenLayout(mode: LayoutMode.phone, columns: 4, margin: 16, content: null);
    } else if (width < 632) {
      return const ScreenLayout(mode: LayoutMode.phone, columns: 4, margin: null, content: 568);
    } else if (width < 905) {
      return const ScreenLayout(mode: LayoutMode.tablet, columns: 8, margin: 32, content: null);
    } else if (width < 1240) {
      return const ScreenLayout(mode: LayoutMode.tablet, columns: 12, margin: null, content: 840);
    } else if (width < 1440) {
      return const ScreenLayout(mode: LayoutMode.desktop, columns: 12, margin: 200, content: null);
    } else {
      return const ScreenLayout(mode: LayoutMode.desktop, columns: 12, margin: null, content: 1040);
    }
  }

  @override
  bool operator ==(Object other) =>
      other is ScreenLayout &&
      other.mode == mode &&
      other.columns == columns &&
      other.margin == margin &&
      other.content == content;

  @override
  int get hashCode => Object.hash(mode, columns, margin, content);

  @override
  String toString() => 'AppScreen(mode: $mode, columns: $columns, margin: $margin, content: $content)';

  static ScreenLayout of(BuildContext context) => context.dependOnInheritedWidgetOfExactType<_AppScreenWrapper>()!.data;
}

enum LayoutMode {
  phone,
  tablet,
  desktop,
}

class _AppScreenWrapper extends InheritedWidget {
  final ScreenLayout data;

  const _AppScreenWrapper({required this.data, required super.child});

  @override
  bool updateShouldNotify(_AppScreenWrapper oldWidget) => data != oldWidget.data;

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<ScreenLayout>('data', data));
  }
}
