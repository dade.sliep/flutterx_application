part of 'application.dart';

/// Initialize application and return fist route
/// Errors here are unhandled
typedef InitializeApp = FutureOr<ActivityRoute> Function(BuildContext context);

/// The returned widget must be or contains in its tree WidgetsApp
/// Use [MaterialApp.router] constructor and pass to it each of the provided parameters
typedef AppFactory = Widget Function({
  required AppRouteInformationProvider routeInformationProvider,
  required AppRouteInformationParser routeInformationParser,
  required AppRouterDelegate routerDelegate,
  required GenerateAppTitle onGenerateTitle,
  required ThemeData theme,
  required TransitionBuilder builder,
  required Locale? locale,
  required Iterable<LocalizationsDelegate>? localizationsDelegates,
  required Iterable<Locale> supportedLocales,
});

/// Application root widget
class Application extends StatefulWidget {
  final String name;
  final InitializeApp? initialize;
  final Set<ActivityRoute> routes;
  final OnUnknownRoute onUnknownRoute;
  final ThemeData theme;
  final LocaleData locale;
  final AppFactory appFactory;

  const Application._({
    required this.name,
    required this.initialize,
    required this.routes,
    required this.onUnknownRoute,
    required this.theme,
    required this.locale,
    required this.appFactory,
  }) : assert(routes.length > 0, 'routes cannot be empty!');

  @override
  ApplicationState createState() => ApplicationState();

  static Future<void> _runApp({
    required String name,
    required InitializeApp? initialize,
    required Set<ActivityRoute> routes,
    required OnUnknownRoute onUnknownRoute,
    required ThemeData theme,
    required LocaleData locale,
    required AppFactory appFactory,
  }) async =>
      runApp(Application._(
          name: name,
          initialize: initialize,
          routes: routes,
          onUnknownRoute: onUnknownRoute,
          theme: theme,
          locale: locale,
          appFactory: appFactory));

  static ApplicationState of(BuildContext context) =>
      requireNotNull(context.findRootAncestorStateOfType<ApplicationState>(),
          message: () => 'Application not reachable from current context. Have you called runApplication()?');

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(StringProperty('name', name))
      ..add(ObjectFlagProperty<InitializeApp>.has('initialize', initialize))
      ..add(IterableProperty<ActivityRoute>('routes', routes))
      ..add(ObjectFlagProperty<OnUnknownRoute>.has('onUnknownRoute', onUnknownRoute))
      ..add(DiagnosticsProperty<ThemeData>('theme', theme))
      ..add(DiagnosticsProperty<LocaleData>('locale', locale))
      ..add(ObjectFlagProperty<AppFactory>.has('appFactory', appFactory));
  }
}

class ApplicationState extends State<Application> {
  late final _AppBindingObserver _binding = _AppBindingObserver(this);
  final MutableLiveData<ScreenLayout> _screen = MutableLiveData.late();
  final LiveData<AppLifecycleState> lifecycle = MutableLiveData.late();
  late final MutableLiveData<String> title = MutableLiveData(value: widget.name)
    ..observeForever((value) => SystemChrome.setApplicationSwitcherDescription(
        ApplicationSwitcherDescription(label: value, primaryColor: Theme.of(context).primaryColor.value)));
  late FlutterView _view;
  late ScaffoldMessengerState _scaffoldMessenger;
  AppRouterDelegate? _appRouter;

  AppRouterDelegate get router => _appRouter!;

  LocaleData get locale => widget.locale;

  ScaffoldMessengerState get scaffoldMessenger => _scaffoldMessenger;

  @override
  void initState() {
    super.initState();
    _binding.init();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _view = View.of(context);
    _updateAppScreen();
  }

  @override
  Widget build(BuildContext context) => ViewModelScope(
      scope: #application,
      child: Builder(builder: (context) {
        final router = _appRouter;
        if (router == null) {
          scheduleMicrotask(() => _initializeApp(context));
          return const SizedBox.shrink();
        }
        return MultipleLiveDataBuilder.with2<ScreenLayout, Locale?>(
            x1: _screen,
            x2: locale.enforcedLocale,
            builder: (context, screen, enforcedLocale) => widget.appFactory(
                routeInformationProvider: router.provider,
                routeInformationParser: router.parser,
                routerDelegate: router,
                onGenerateTitle: (_) => title.value,
                theme: widget.theme,
                builder: (context, child) {
                  _scaffoldMessenger = ScaffoldMessenger.of(context);
                  return _AppScreenWrapper(data: screen, child: child ?? const SizedBox.shrink());
                },
                locale: enforcedLocale,
                localizationsDelegates: locale._localizationDelegates,
                supportedLocales: locale.supportedLocales));
      }));

  @override
  void dispose() {
    _binding.dispose();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<LiveData<AppLifecycleState>>('lifecycle', lifecycle))
      ..add(DiagnosticsProperty<MutableLiveData<String>>('title', title))
      ..add(DiagnosticsProperty<LocaleData>('locale', locale))
      ..add(DiagnosticsProperty<ScaffoldMessengerState>('scaffoldMessenger', scaffoldMessenger));
  }

  void restart() {
    _appRouter?.parser.clearStorage();
    runApp(const SizedBox.shrink());
    Application._runApp(
        name: widget.name,
        initialize: widget.initialize,
        routes: widget.routes,
        onUnknownRoute: widget.onUnknownRoute,
        theme: widget.theme,
        locale: widget.locale,
        appFactory: widget.appFactory);
  }

  MaterialBanner? _activeBanner;

  Future<MaterialBannerClosedReason> showMessage(Message message) {
    if (_activeBanner != null) _scaffoldMessenger.hideCurrentMaterialBanner(reason: MaterialBannerClosedReason.remove);
    late final MaterialBanner banner;
    return _scaffoldMessenger
        .showMaterialBanner(banner = _activeBanner = message.build(_scaffoldMessenger.context,
            hide: (reason) => mounted && banner == _activeBanner
                ? _scaffoldMessenger.hideCurrentMaterialBanner(reason: reason)
                : null))
        .closed
        .whenComplete(() => banner == _activeBanner ? _activeBanner = null : null);
  }

  void _updateAppScreen() => _screen.value = ScreenLayout.fromSize(_view.physicalSize / _view.devicePixelRatio);

  Future<void> _initializeApp(BuildContext context) async {
    final initialRoute = await widget.initialize?.call(context) ?? widget.routes.first;
    setState(() {
      _appRouter = AppRouterDelegate(
          context, widget.routes, initialRoute, widget.onUnknownRoute, kIsWeb ? sessionStorage : null, 'router')
        ..addListener(() {
          final title = _appRouter!.currentConfiguration?.name;
          if (title != null) this.title.value = title;
        });
    });
  }
}
