import 'package:flutter_web_plugins/flutter_web_plugins.dart';

class FullPathUrlStrategy extends PathUrlStrategy {
  final PlatformLocation _platformLocation;

  FullPathUrlStrategy([super.platformLocation = const BrowserPlatformLocation()])
      : _platformLocation = platformLocation;

  @override
  String getPath() => super.getPath() + (_platformLocation.hash ?? '');
}
