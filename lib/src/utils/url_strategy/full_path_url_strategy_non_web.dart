import 'package:flutter_web_plugins/url_strategy.dart';

class FullPathUrlStrategy extends PathUrlStrategy {
  FullPathUrlStrategy([super.platformLocation]);
}
