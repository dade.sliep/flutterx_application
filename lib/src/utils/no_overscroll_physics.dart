import 'package:flutter/widgets.dart';

/// [ClampingScrollPhysics] but without overscroll
class NoOverScrollPhysics extends ClampingScrollPhysics {
  const NoOverScrollPhysics({super.parent});

  @override
  NoOverScrollPhysics applyTo(ScrollPhysics? ancestor) => NoOverScrollPhysics(parent: buildParent(ancestor));

  @override
  double adjustPositionForNewDimensions({
    required ScrollMetrics oldPosition,
    required ScrollMetrics newPosition,
    required bool isScrolling,
    required double velocity,
  }) =>
      super
          .adjustPositionForNewDimensions(
              oldPosition: oldPosition, newPosition: newPosition, isScrolling: isScrolling, velocity: velocity)
          .clamp(newPosition.minScrollExtent, newPosition.maxScrollExtent);
}
