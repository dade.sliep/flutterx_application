import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_preferences/flutterx_preferences.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

typedef RefreshToken<T extends Object> = FutureOr<T> Function(T token);
typedef TokenValidPredicate<T extends Object> = bool Function(T token);
typedef TokenAuthorize<T extends Object> = FutureOr<String> Function(T token);

class TokenHandler<T extends Object> {
  final Mutex _lock = Mutex();
  final StorageMutableLiveData<T?> _token;
  late final LiveData<T?> token = _token.transform((value) => value, reactive: true);
  late final LiveData<bool> hasToken = _token.transform((value) => value != null, reactive: true);
  final String key;
  final Storage storage;
  final TokenValidPredicate<T> isTokenValid;
  final TokenAuthorize<T> authorize;
  final RefreshToken<T>? refreshToken;

  Future<String?> get authorization => _lock.lock(() async {
        var token = _token.value;
        if (token == null) return null;
        if (!isTokenValid(token)) {
          final refresh = refreshToken;
          if (refresh == null) {
            _token.value = null;
            throw TokenExpiredException._(token, 'Token expired');
          }
          try {
            token = _token.value = await refresh(token);
          } catch (e) {
            _token.value = null;
            throw TokenExpiredException._(token!, 'Token refresh failed', e);
          }
        }
        return authorize(token);
      });

  TokenHandler({
    required this.key,
    T? defaultValue,
    required this.storage,
    dynamic Function(T value)? toJson,
    required T Function(Map<String, dynamic> json) fromJson,
    required this.isTokenValid,
    required this.authorize,
    this.refreshToken,
  }) : _token = StorageMutableLiveData.jsonObject(
            key: key,
            defaultValue: defaultValue,
            storage: storage,
            toJson: toJson == null ? null : (value) => toJson(value as T),
            fromJson: fromJson);

  Future<void> updateToken(ValueGetter<FutureOr<T>> token) =>
      _lock.lock<void>(() async => _token.value = await token());

  Future<void> clear() => _lock.lock(() {
        _token.reset();
        return SynchronousFuture(null);
      });
}

class TokenExpiredException implements Exception {
  final Object _token;
  final String reason;
  final Object? cause;

  TokenExpiredException._(this._token, this.reason, [this.cause]);

  T token<T extends Object>() => _token as T;

  @override
  String toString() => 'TokenExpiredException: $reason${cause == null ? '' : '\n$cause'}';
}
