import 'package:flutter/widgets.dart';

/// [FocusNode] but cannot have focus
///
/// Can be used to disable focus on text fields
class NoFocusNode extends FocusNode {
  NoFocusNode({
    super.skipTraversal = true,
    super.canRequestFocus = false,
    super.descendantsAreFocusable = false,
  });

  @override
  bool get hasFocus => false;

  @override
  bool get hasPrimaryFocus => false;
}
