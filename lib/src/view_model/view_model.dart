import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

part 'view_model_scope.dart';
part 'view_model_task.dart';

/// The ViewModel class is designed to store and manage data in a lifecycle conscious way.
/// ViewModel is a class that is responsible for preparing and managing the data for a Widget tree.
/// It also handles the communication of the Widgets with the rest of the application (e.g. calling the
/// business logic classes).
/// A ViewModel is always created in association with a scope (defined with the use of the related [ViewModelScope]
/// widget) and will be retained as long as the scope is alive (until unmount).
/// The purpose of the ViewModel is to acquire and keep the information that is necessary for your widgets.
/// This widgets should be able to observe changes in the ViewModel.
/// ViewModels usually expose this information via [LiveData].
/// ViewModel's only responsibility is to manage the data for the UI.
/// It should never access your view hierarchy or hold a reference back to the widgets tree.
///
/// Here is an example of how a correct ViewModel implementation looks like:
///   MyViewModel._(super.provider);
///
///   factory MyViewModel.of(BuildContext context) => ViewModelProvider.of(context).provide(MyViewModel._);
abstract class ViewModel {
  ViewModelProvider? _provider;

  ViewModel(ViewModelProvider this._provider);

  @protected
  ViewModelProvider get provider => requireNotNull(_provider, message: () => '$this is not attached to a provider');

  @protected
  BuildContext get context => provider.context;

  @protected
  @mustCallSuper
  void onCleared() => _provider = null;
}

/// Factory used to instantiate your ViewModel.
/// The best practice is defining the ViewModel constructor as private and exposing a static parameterless method that
/// returns the newly constructed instance of your ViewModel. You should pass only that method as [ViewModelFactory]
/// parameter
typedef ViewModelFactory<T extends ViewModel> = T Function(ViewModelProvider provider);

/// The provider interface provides you the ViewModel
abstract class ViewModelProvider {
  const ViewModelProvider._();

  /// Provide ViewModel [BuildContext].
  /// This should only be used for dependency injection:
  /// - Do NOT reference UI components from ViewModel
  /// - Do NOT write UI logic inside your ViewModel
  BuildContext get context;

  /// Provide a [VM] instance.
  /// The first time you call this method on [VM], [factory] will be called to create the ViewModel instance which
  /// will be retained for future calls until scope disposition
  /// Type argument should exactly match returned instance type
  VM provide<VM extends ViewModel>(ViewModelFactory<VM> factory);

  /// Clears all ViewModel instances that belongs to this provider
  void clear();

  static ViewModelProvider ofScope(Symbol scope) => GlobalObjectKey(scope).currentContext as ViewModelProvider;

  static ViewModelProvider of(BuildContext context) =>
      context.getElementForInheritedWidgetOfExactType<ViewModelScope>() as ViewModelProvider;
}
