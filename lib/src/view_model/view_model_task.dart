part of 'view_model.dart';

mixin TaskViewModel on ViewModel {
  final Map<Object?, LiveData<bool>> _loading = {};
  final Map<TaskViewModel, ObserverWrapper> _dependant = {};

  /// Active tasks mapped by tag
  final LiveMap<Object?, Set<TaskWrapper>> activeTasks = LiveMap();

  /// Loading state for all tasks
  LiveData<bool> get loading => _loading[null] ??= activeTasks.transform<bool>((tasks) => tasks.isNotEmpty);

  /// Loading state for tasks with given [tag]
  LiveData<bool> loadingWithTag(Object tag) =>
      _loading[tag] ??= activeTasks.transform<bool>((tasks) => tasks[tag]?.isNotEmpty ?? false);

  /// Make this loading[tag] depend on [tasks] loading[childTag]
  /// This means that if at least one task is currently active on child [tasks] also this will have one task active
  @protected
  void loadingDependsOn(TaskViewModel tasks, {Object? childTag, Object? tag}) {
    final loading = childTag == null ? tasks.loading : tasks.loadingWithTag(childTag);
    final task = TaskWrapper._(SynchronousFuture(tasks), {'_tag': tag ?? tasks.runtimeType});
    tasks._dependant[this] =
        loading.observeForever((value) => value ? activeTasks.addWithTag(task) : activeTasks.removeWithTag(task));
  }

  /// Add [task] to this viewModel with given [tag]
  /// Do not call this method directly, prefer using [FutureExt.notify]
  @protected
  Future<T> addTask<T>(Future<T> task, {JsonObject? args, Object? tag}) async {
    final wrapper = TaskWrapper._(task, {...?args, '_tag': tag});
    if (!activeTasks.addWithTag(wrapper)) return task;
    try {
      final result = await task;
      handleTaskSuccess(wrapper);
      return result;
    } catch (error, stackTrace) {
      await handleTaskError(wrapper, error, stackTrace);
      rethrow;
    } finally {
      activeTasks.removeWithTag(wrapper);
    }
  }

  @protected
  FutureOr<void> handleTaskSuccess(TaskWrapper task) {
    for (final tasks in _dependant.keys) tasks.handleTaskSuccess(task);
  }

  @protected
  FutureOr<void> handleTaskError(TaskWrapper task, Object error, StackTrace stackTrace) {
    for (final tasks in _dependant.keys) tasks.handleTaskError(task, error, stackTrace);
  }

  @override
  void onCleared() {
    activeTasks.clear();
    for (final key in [..._dependant.keys]) _dependant.remove(key)!.dispose();
    super.onCleared();
  }
}

extension FutureExt<T> on Future<T> {
  /// Notify given [viewModel] of this future task
  Future<T> notify(TaskViewModel viewModel, {JsonObject? args, Object? tag}) =>
      viewModel.addTask(this, args: args, tag: tag);
}

extension _TagMapExt on LiveMap<Object?, Set<TaskWrapper>> {
  bool addWithTag(TaskWrapper task) => apply((data) => (data[task.tag] ??= {}).add(task));

  bool removeWithTag(TaskWrapper task) => apply((data) {
        final tag = task.tag;
        final set = data[tag];
        if (set == null || !set.remove(task)) return false;
        if (set.isEmpty) data.remove(tag);
        return true;
      });
}

@immutable
class TaskWrapper {
  final Future task;
  final JsonObject args;

  Object? get tag => args['_tag'];

  const TaskWrapper._(this.task, this.args);

  @override
  bool operator ==(Object other) => other is TaskWrapper && task == other.task;

  @override
  int get hashCode => task.hashCode;
}
