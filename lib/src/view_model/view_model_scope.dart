part of 'view_model.dart';

/// Define a ViewModel scope.
/// All descendants of this widget will have access to the [ViewModelProvider] of this scope through it's context
/// Use cases of scoped viewModels
///     MyViewModel.of(context) // scope: <global>
///
///     ViewModelScope.builder(
///       scope: #my_scope,
///       builder: (context) => MyViewModel.of(context)); // scope: #my_scope
///
///     ViewModelScope.builder(
///       scope: #my_scope,
///       builder: (context) => ViewModelScope.builder(
///         scope: #my_scope_2,
///         builder: (context) => MyViewModel.of(context))); // scope: #my_scope_2
///
///     ViewModelScope.builder(
///       scope: #my_scope,
///       builder: (context) => ViewModelScope.builder(
///         scope: #my_scope_2,
///         builder: (context) => MyViewModel.ofScope(#my_scope))); // scope: #my_scope
class ViewModelScope extends InheritedWidget {
  final Symbol scope;

  ViewModelScope({required this.scope, required super.child}) : super(key: GlobalObjectKey(scope));

  ViewModelScope.builder({required Symbol scope, required WidgetBuilder builder})
      : this(scope: scope, child: Builder(builder: builder));

  @override
  bool updateShouldNotify(ViewModelScope oldWidget) => oldWidget.scope != scope;

  @override
  InheritedElement createElement() => _ViewModelScopeElement(this);

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Symbol>('scope', scope));
  }
}

class _ViewModelScopeElement extends InheritedElement implements ViewModelProvider {
  final Map<Type, ViewModel> _viewModels = {};

  @override
  ViewModelScope get widget => super.widget as ViewModelScope;

  @override
  BuildContext get context => this;

  _ViewModelScopeElement(ViewModelScope super.widget);

  @override
  VM provide<VM extends ViewModel>(ViewModelFactory<VM> factory) {
    final result = _viewModels[VM] ??= factory(this);
    assert(result.runtimeType == VM, 'VM type parameter is implicit or does not match ViewModel type');
    return result as VM;
  }

  @override
  void unmount() {
    clear();
    super.unmount();
  }

  @override
  void clear() {
    for (final key in [..._viewModels.keys]) _viewModels.remove(key)?.onCleared();
  }
}
