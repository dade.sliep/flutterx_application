import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

/// Interface for localizing your application in a efficient way:
///
/// First: declare your interface:
/// ```dart
/// late Labels labels;
///
/// class Labels extends LabelInterface {
///   static final Set<Labels> supportedLabels = {_it, _en};
///   static final LiveEvent<Labels> onLabel = LiveEvent()..observeForever((value) => labels = value);
///   final String appName;
///
///   const Labels._({
///     required super.locale,
///     required this.appName,
///   });
///
/// }
/// ```
///
/// Then define an implementation for every language:
/// ```dart
/// final _en = Labels._(
///   locale: const Locale('en', 'US'),
///   appName: 'Application Demo',
/// );
/// ```
///
/// Finally pass Labels.supportedLabels and Labels.onLabel to corresponding parameters of [runApplication]
///
/// And then you're ready to use labels.appName without worrying about translations
@immutable
abstract class LabelInterface {
  final Locale locale;

  const LabelInterface({required this.locale});
}

/// Used internally by [Application] you may not interact with this class directly
@immutable
class LabelInterfaceDelegate<T extends LabelInterface> extends LocalizationsDelegate<T> {
  final Set<T> labels;
  final ValueSetter<T> onLabel;

  const LabelInterfaceDelegate(this.labels, this.onLabel);

  @override
  bool isSupported(Locale locale) => labels.any((label) => label.locale.languageCode == locale.languageCode);

  @override
  Future<T> load(Locale locale) {
    final label = labels.firstWhere((label) => label.locale.languageCode == locale.languageCode);
    onLabel(label);
    return SynchronousFuture(label);
  }

  @override
  bool shouldReload(LabelInterfaceDelegate<T> old) => !setEquals(old.labels, labels) || old.onLabel != onLabel;

  @override
  bool operator ==(Object other) =>
      other is LabelInterfaceDelegate && setEquals(other.labels, labels) && other.onLabel == onLabel;

  @override
  int get hashCode => Object.hash(Object.hashAll(labels), onLabel);
}

/// Get flag emoji of Locale
extension LocaleExt on Locale {
  String get flag => requireNotNull(countryCode, message: () => 'Country code was not provided to this Locale')
      .toUpperCase()
      .replaceAllMapped(RegExp('[A-Z]'), (match) => String.fromCharCode(match[0]!.codeUnitAt(0) + 127397));
}
