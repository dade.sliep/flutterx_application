/// Flutterx Application
library flutterx_application;

export 'src/application/application.dart';
export 'src/fragment/fragment.dart';
export 'src/localization/labels.dart';
export 'src/routing/app_route.dart';
export 'src/utils/no_focus_node.dart';
export 'src/utils/no_overscroll_physics.dart';
export 'src/utils/token_handler.dart';
export 'src/utils/url_strategy/full_path_url_strategy_non_web.dart'
    if (dart.library.html) 'src/utils/url_strategy/full_path_url_strategy.dart';
export 'src/view_model/view_model.dart';
