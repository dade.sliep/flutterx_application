# flutterx_application

Core functionalities of a flutter application including translations, lifecycle events, navigation, dialogs

## Import

Import the library like this:

```dart
import 'package:flutterx_application/flutterx_application.dart';
```

## Usage

Check the documentation in the desired source file of this library